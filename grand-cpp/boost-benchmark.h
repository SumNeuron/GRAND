#ifndef BOOST_BENCHMARK_H
#define BOOST_BENCHMARK_H

using VertexName = GraphUtil::boost::VertexName;
using EdgeWeight = GraphUtil::boost::EdgeWeight;
using Vertex = GraphUtil::boost::Vertex;
using Edge = GraphUtil::boost::Edge;
using GraphProperties = GraphUtil::boost::GraphProperties;
using Graph = GraphUtil::boost::Graph;
using VertexDescriptor = GraphUtil::boost::VertexDescriptor;

#endif
