#include <iostream>
#include <fstream>
#include <sstream>
#include <cassert>
#include <limits>
#include <vector>

// UNIX-specific stuff
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/wait.h>


#include "grand-astar.h"
#include "timetaker.h"

const int MAXIMUM_PATH_LENGTH = 3;

TimeTaker time_taker;

//TODO convert as many pointers as possible to references because this is C++ and not C

int ignore_result; //dummy variable to tell the compiler that things will be fine pretty much every time

/* default constructor, so that the C++ STL map is happy */
Vertex::Vertex() {
        //std::cout << "Called default constructor of Vertex()" << std::endl;
        this -> name = "";
}

Vertex::Vertex( VertexName name ) {
        this -> name = name;
}

Vertex * Graph::get_vertex( VertexName name ) {
        if( this -> vs.count( name ) ) {
                return &( this -> vs[ name ] ); //element exists, just return it
        } //otherwise construct it and then return it
        this -> vs[ name ] = Vertex( name );
        return &( this -> vs[ name ] );
}



/*
 *File format: Every line an edge from A to B with a given weight.

A       B       weight

Note that this function only does little error handling. It trusts the input.
*/
Graph::Graph( char * filename ) {
        auto vs = this -> vs;

        std::string line;
        std::ifstream file( filename );
        if ( !file.is_open() ) {
                perror( "Function Graph::Graph| error while opening file" );
        }
        bool expect_file_to_end = false;
        while( !file.eof() ) {
                assert( !expect_file_to_end ); //if this is false, then the file format is bad or there is a bug in the program

                VertexName vertex_A_name, vertex_B_name;
                EdgeWeight edge_weight;

                file >> vertex_A_name;
                file >> vertex_B_name;
                file >> edge_weight;

                if( vertex_A_name == "" || vertex_B_name == "" ) {
                        expect_file_to_end = true;
                        continue; //most likely there was an unecessary newline before the EOF
                                  //if that should not be the case, the assert
                                  //at the beginning of this loop will strike
                }

                Vertex * vertex_A = this -> get_vertex( vertex_A_name );
                Vertex * vertex_B = this -> get_vertex( vertex_B_name );

                vertex_A -> successors[ vertex_B_name ] = edge_weight;
                vertex_B -> predecessors[ vertex_A_name ] = edge_weight;
        }

        if ( file.bad() ) {
                perror( "Function Graph::Graph| error while reading file" );
        }
}

//this function exists mostly for debugging
void Graph::list_edges() {
        for( auto it = this -> vs.begin();
             it != vs.end();
             ++it ) {
                for( auto it_successors = it -> second.successors.begin();
                     it_successors != it -> second.successors.end();
                     ++it_successors ) {

                        EdgeWeight weight = it_successors -> second;
                        std::cout << it -> first << " -[" << weight << "]-> " << it_successors -> first << std::endl;
                }
                std::cout << "------" << std::endl;
                for( auto it_predecessors = it -> second.predecessors.begin();
                     it_predecessors != it -> second.predecessors.end();
                     ++it_predecessors ) {

                        EdgeWeight weight = it_predecessors -> second;
                        std::cout << it -> first << " <-[" << weight << "]- " << it_predecessors -> first << std::endl;
                }
                std::cout << "======" << std::endl;
        }
}

Vertex * Graph::getClosestVertexForAStar( VertexNameSet * open_set,
                                          std::unordered_map<VertexName,double> * f_scores ) {

	//TimeTaker timer;
        assert( !open_set -> empty() );
        double minimum = std::numeric_limits<double>::infinity();
        VertexName best_vertex;
        for( auto it = open_set -> begin();
             it != open_set -> end();
             ++it ) {

                VertexName vertex_name = *it;
                if( f_scores -> count( vertex_name ) ) {
                        double f_score = (*f_scores)[ vertex_name ];
                        if( f_score < minimum ) {
                                minimum = f_score;
                                best_vertex = vertex_name;
                        }
                } //else treat the f_score like infinity
        }

	//timer.time_passed("looked up closest vertex");
        return this -> get_vertex( best_vertex );
}

void Graph::a_star_depth_lock(
        std::vector<VertexName> * out_result,
        VertexName start,
        VertexName stop,
        DistanceFunction real_distance,
        DistanceFunction heuristic_distance,
        int max_steps_from_start,
        SearchDirection direction ) {

        Vertex * destination = this -> get_vertex( stop );

        std::unordered_map<VertexName, VertexName> backtracking;
        
        VertexNameSet closed_set, open_set;
        open_set.insert( start );

        std::unordered_map<VertexName,int> steps_from_start;
        steps_from_start[ start ] = 0;

        std::unordered_map<VertexName,double> f_scores; //real score plus heuristic distance to destination
        std::unordered_map<VertexName,double> g_scores; //real score
        f_scores[ start ] = 1; //TODO why 1 and not 0?

        bool found_path = false;

	TimeTaker tt;
	tt.verbose = false;
        while( !open_set.empty() ) {

                Vertex * current_vertex = this -> getClosestVertexForAStar( &open_set, &f_scores );
		tt.time_passed( "getting closest vertex" );
                VertexName current_vertex_name = current_vertex -> name;

                /* make sure the vertex is marked as processed */
                open_set.erase( current_vertex_name );
                closed_set.insert( current_vertex_name );

                if( steps_from_start[ current_vertex_name ] > max_steps_from_start ) {
                        continue; //too far away. No further processing reasonable
                }

                if( current_vertex_name == stop ) {
                        found_path = true;
                        break;
                }

                std::unordered_map<VertexName,EdgeWeight> * directional_neighbors;
                if( direction == SearchDirection::Successors ) {
                        directional_neighbors = &current_vertex -> successors;
                } else {
                        directional_neighbors = &current_vertex -> predecessors;
                }

		tt.time_passed( "pre-loop" );
                for( auto it = directional_neighbors -> begin();
                     it != directional_neighbors -> end();
                     ++it ) {

                        VertexName neighbor_name = it -> first;
                        Vertex * neighbor = this -> get_vertex( neighbor_name );
			tt.time_passed( "getting vertex" );
                        if( closed_set.count( neighbor_name ) ) {
                                continue; //already processed
                        }

                        if( open_set.count( neighbor_name ) == 0 ) {
                                open_set.insert( neighbor_name );
                                steps_from_start[ neighbor_name ] = steps_from_start[ current_vertex_name ] + 1;
                        }
			tt.time_passed( "checking open set" );

                        double tentative_g_score = g_scores[ current_vertex_name ] + real_distance( this, current_vertex, neighbor );
			tt.time_passed( "tentative_g_score" );
                        if( g_scores.count( neighbor_name ) && tentative_g_score >= g_scores[ neighbor_name ] ) {
                                continue;
                        }
                        g_scores[ neighbor_name ] = tentative_g_score;
                        f_scores[ neighbor_name ] = tentative_g_score + heuristic_distance( this, neighbor, destination );
			tt.time_passed( "f_scores" );

                        backtracking[ neighbor_name ] = current_vertex_name;
			tt.time_passed( "assign backtracking" );
                }
        }

	tt.time_passed( "done with while loop" );
	if( found_path ) {
		determine_final_a_star_path( out_result, &backtracking, stop );
	} //else just leave out_result unchanged
	tt.time_passed( "done with final A* path" );
	tt.print_max_diff();
}

void determine_final_a_star_path(
        std::vector<VertexName> * out_result,
        std::unordered_map<VertexName,VertexName> * backtracking,
        VertexName stop ) {

        VertexName current = stop;
        out_result -> push_back( current );

        while( backtracking -> count( current ) ) {
                current = (*backtracking)[ current ];
                out_result -> push_back( current );
        }
}

void make_fd_non_blocking( int fd ) { //fd = (UNIX) file descriptor
	//see also: http://www.linux-mag.com/id/308/
	fcntl( fd,
	       F_SETFL, //set flags
	       fcntl( fd, F_GETFL ) | O_NONBLOCK //get current flags and add non-blocking
        );
}

double real_dist_fn( Graph * g, Vertex * a, Vertex * b ) {
        return 1.0;
}

double heur_dist_fn( Graph * g, Vertex * a, Vertex * b ) {
        return 0.0;
}

int main( int argc, char ** argv ) {
        if( argc < 4 ) {
                std::cerr
			<< "Usage: grand connections-file knockout de-gene1 [de-gene2...]"
			<< std::endl
			<< std::endl
			<< "Output line format: [PATH] forward path <=> backward path"
			<< std::endl;
                return -1;
        }

	time_taker.time_passed( "greetings" );
        Graph g( argv[ 1 ] );
	time_taker.time_passed( "graph constructed" );
        std::string knockout( argv[ 2 ] );

	int all_pipe_fds[ argc - 3 ];
	pid_t all_child_process_ids[ argc - 3 ];

	for( int i = 3; i < argc; i++ ) {
		std::string de_gene( argv[ i ] );

		//Setting up interprocess communication channels
		int pipe_fds[ 2 ]; //child uses fd 1 (writing) and parent uses fd 0 (reading)
		ignore_result = pipe( pipe_fds );
		make_fd_non_blocking( pipe_fds[ 0 ] );
		make_fd_non_blocking( pipe_fds[ 1 ] );

		pid_t child_pid = fork();
		if( child_pid ) { //parent process
			time_taker.time_passed( "forked and in parent" );
			int idx = i - 3;
			close( pipe_fds[ 1 ] ); //no writing from parent
			all_pipe_fds[ idx ] = pipe_fds[ 0 ];
			all_child_process_ids[ idx ] = child_pid;
			printf( "[INFO] Determining paths between %s and %s\n", knockout.c_str(), de_gene.c_str() );
		} else { //child
			time_taker.time_passed( "forked and in child" );
			close( pipe_fds[ 0 ] ); //no reading from child

			//we need to do further fork so that we can do the
			//forward search and the backwards search in parallel
			child_pid = fork();
			time_taker.time_passed(child_pid?"initiated backward search": "initiated forward search");

			std::vector<VertexName> path;

			g.a_star_depth_lock(
				&path,
				child_pid? de_gene: knockout, //if we are the (inner) parent, we do a DE-> KO search. Otherwise it is KO-> DE
				child_pid? knockout: de_gene, //see above
				real_dist_fn,
				heur_dist_fn,
				MAXIMUM_PATH_LENGTH,
				SearchDirection::Successors );
			time_taker.time_passed(child_pid?"done backward search": "done forward search");

			if( child_pid ) { //the parent should wait for the child
				waitpid( child_pid, NULL, 0 );
				//at this point the child should have written
				//the path to the file descriptor and we can add
				//"<=>" and the backward path
				ignore_result = write( pipe_fds[ 1 ], "<=> ", 4 );
			}
			for( auto it = path.rbegin();
			     it != path.rend();
			     ++it ) {
				const char * gene_name = it -> c_str();
				ignore_result = write( pipe_fds[ 1 ], gene_name, it -> size() );
				ignore_result = write( pipe_fds[ 1 ], " ", 1 );
			}


			if( child_pid ) { //only the parent should close the file
				close( pipe_fds[ 1 ] );
			}
			time_taker.time_passed(child_pid?"done backward search process (with waiting for the forward search process)": "done forward search process");
			time_taker.print_max_diff();
			return 0; //exit both search processes
		}
	}
	puts( "[INFO] Submitted calculation for all paths" );

	char buf[ 32 ];
	int n_chars_read = 0;
	for( int i = 3; i < argc; i++ ) {
		int idx = i - 3;
		pid_t child_pid = all_child_process_ids[ idx ];
		waitpid( child_pid, NULL, 0 );
		printf( "[PATH] " );

		while( ( n_chars_read = read( all_pipe_fds[ idx ], buf, 31 ) ) ) {
			buf[ n_chars_read ] = 0; //terminate string
			printf( "%s", buf );
		}
		puts( "" ); //new line
	}
	puts( "[DONE]" );
	time_taker.time_passed( "done parent" );
	time_taker.print_max_diff();
}
