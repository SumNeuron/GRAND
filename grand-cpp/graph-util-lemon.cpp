#include <cassert>

#include "graph-util-lemon.h"

namespace GraphUtil {
	namespace lemon {
		VertexDescriptor get_vertex_index_with_name( Graph & graph, std::string & vertex_name, VertexMapping & vertex_descriptors ) {
			if( vertex_descriptors.count( vertex_name ) == 0 ) {
				VertexDescriptor new_vertex = graph.addNode();
				//graph_bundle.vertex_names[ vertex_name ] = new_vertex;
				vertex_descriptors[ vertex_name ] = new_vertex; //assign to global variable
				return new_vertex;
			}
			return vertex_descriptors[ vertex_name ];
		}

		void read_graph_from_file(
			std::string filename,
			Graph & out_graph,
			Graph::NodeMap<std::string> & vertex_names,
			VertexMapping & vertex_descriptors ) {
			std::string line;
			std::ifstream file( filename );
			if ( !file.is_open() ) {
				perror( "Function Graph::Graph| error while opening file" );
			}
			bool expect_file_to_end = false;
			while( !file.eof() ) {
				assert( !expect_file_to_end ); //if this is false, then the file format is bad or there is a bug in the program

				VertexName vertex_A_name, vertex_B_name;
				EdgeWeight edge_weight;

				file >> vertex_A_name;
				file >> vertex_B_name;
				file >> edge_weight;

				if( vertex_A_name == "" || vertex_B_name == "" ) {
					expect_file_to_end = true;
					continue; //most likely there was an unecessary newline before the EOF
						  //if that should not be the case, the assert
						  //at the beginning of this loop will strike
				}

				VertexDescriptor vertex_a = get_vertex_index_with_name( out_graph, vertex_A_name, vertex_descriptors );
				VertexDescriptor vertex_b = get_vertex_index_with_name( out_graph, vertex_B_name, vertex_descriptors );

				//this might be a bit wasteful because it might overwrite
				//existing entries sometimes
				vertex_names[ vertex_a ] = vertex_A_name; //store for human-readable backtracking
				vertex_names[ vertex_b ] = vertex_B_name; //store for human-readable backtracking

				out_graph.addArc( vertex_a, vertex_b );
			}

			if( file.bad() ) {
				std::perror( "Function read_graph_from_file| error while reading file" );
			}
		}
	}
}
