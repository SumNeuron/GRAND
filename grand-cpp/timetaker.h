#ifndef TIMETAKER_H
#define TIMETAKER_H

#include <string>
#include <chrono>

using PointInTime = std::chrono::system_clock::time_point;
using Duration = std::chrono::milliseconds;

PointInTime walltime();
class TimeTaker {
	PointInTime now;
	Duration max_diff;
	std::string max_diff_label;
	public:
	TimeTaker();
	bool verbose;
	void time_passed( std::string label );
	void print_max_diff();
};

#endif
