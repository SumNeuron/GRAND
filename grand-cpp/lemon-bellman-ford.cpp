#include <unordered_map>
#include "graph-util-lemon.h"

using Graph = GraphUtil::lemon::Graph;
using VertexDescriptor = GraphUtil::lemon::VertexDescriptor;
using VertexName = GraphUtil::lemon::VertexName;


/**
 * Find up to n paths of maximum length k between two vertices s and t.
 *
 * @param graph The graph in which the paths should be founds
 * @param source The starting node (s) from which paths should be found
 * @param target The destination node (t) to which paths should be found
 * @param maximum_number_of_paths (n) the number of paths that should be found
 * @param maximum_path_length (k) the maximum length of paths between two nodes
 *
 */
void findPathsWithOverlap( Graph graph,
		           VertexDescriptor source,
		           VertexDescriptor target,
		           unsigned int maximum_number_of_paths,
			   unsigned int maximum_path_length ) {

}


int main( int argc, char ** argv ) {
	if( argc < 3 ) {
		std::cerr << "Usage: lemon-bellman-ford pathway_commons_full_graph.tab KO [DE ...]" << std::endl;
		return 1;
	}

	std::unordered_map<VertexName,VertexDescriptor> vertex_descriptors;

	Graph g;
	Graph::NodeMap<std::string> vertex_name( g );
	GraphUtil::lemon::read_graph_from_file( argv[ 1 ], g, vertex_name, vertex_descriptors ); //sets up g and vertex_descriptors
	
}
