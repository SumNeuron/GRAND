#ifndef LEMON_BENCHMARK_H
#define LEMON_BENCHMARK_H

#include "graph-util-lemon.h"

using Graph = GraphUtil::lemon::Graph;
using NodeMap = GraphUtil::lemon::NodeMap;
using VertexName = GraphUtil::lemon::VertexName;
using EdgeWeight = GraphUtil::lemon::EdgeWeight;
using VertexDescriptor = GraphUtil::lemon::VertexDescriptor;
#endif
