
#include <fstream>
#include <unordered_map>
#include <cassert>

#include <lemon/list_graph.h>
#include <lemon/bfs.h>
#include <lemon/lgf_reader.h>

#include "timetaker.h"
#include "graph-util-lemon.h"
#include "lemon-benchmark.h"

int main( int argc, char ** argv ) {
	if( argc < 3 ) {
		std::cerr << "Usage: lemon-benchmark pathway_commons_full_graph.tab KO [DE ...]" << std::endl;
		return 1;
	}

	std::unordered_map<VertexName,VertexDescriptor> vertex_descriptors;

	Graph g;
	Graph::NodeMap<std::string> vertex_name( g );
	GraphUtil::lemon::read_graph_from_file( argv[ 1 ], g, vertex_name, vertex_descriptors ); //sets up g and vertex_descriptors


	Graph::Node ko_node = vertex_descriptors[ argv[ 2 ] ];

	lemon::Bfs<lemon::ListDigraph> bfs( g );
	bfs.run( ko_node );

	for( int i = 3; i < argc; i++ ) {
		lemon::ListDigraph::Node current_node = vertex_descriptors[ argv[ i ] ];
		while( current_node != ko_node && current_node != lemon::INVALID ) {
			std::cout << vertex_name[ current_node ] << " ";
			current_node = bfs.predNode( current_node );
		}
		if( current_node == lemon::INVALID ) {
			std::cout << std::endl; //there is no path from start to finish
		} else {
			std::cout << vertex_name[ current_node ] << std::endl;
		}
	}

	for( int i = 3; i < argc; i++ ) {
		lemon::ListDigraph::Node de_node = vertex_descriptors[ argv[ i ] ];
//lemon::Bfs<lemon::ListDigraph> bfs( g ); //TODO remove me
		bfs.run( de_node, ko_node );
		lemon::ListDigraph::Node current_node = ko_node;
		while( current_node != de_node && current_node != lemon::INVALID ) {
			std::cout << vertex_name[ current_node ] << " ";
			current_node = bfs.predNode( current_node );
		}
		if( current_node == lemon::INVALID ) {
			std::cout << std::endl; //there is no path from start to finish
		} else {
			std::cout << vertex_name[ current_node ] << std::endl;
		}
	}

	return EXIT_SUCCESS;
}
