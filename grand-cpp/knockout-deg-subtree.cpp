#include <vector>
#include <cstdlib>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>

#include <lemon/list_graph.h>
#include <lemon/adaptors.h>

#include <lemon/bfs.h>
#include <lemon/dijkstra.h>
#include <lemon/bellman_ford.h>

#include "graph-util-lemon.h"
#include "knockout-deg-subtree.h"

using Graph = GraphUtil::lemon::Graph;
using VertexDescriptor = GraphUtil::lemon::VertexDescriptor;
using ArcDescriptor = Graph::Arc;
using GraphCopier = lemon::DigraphCopy<Graph,Graph>;
using VertexName = std::string;

using ReverseGraph = lemon::ReverseDigraph<Graph>; 

using FilterGraph = lemon::FilterNodes<Graph>;
using FilterReverseGraph = lemon::FilterNodes<ReverseGraph>;

using ArcWeight = double;

bool in_vector( std::vector<VertexDescriptor> & vec, VertexDescriptor & vertex ) {
	//I would rarther use std::unordered_set instead of std::vector, but the former has issues with VertexDescriptor
	return std::find( vec.begin(), vec.end(), vertex ) != vec.end();
}

template <class SEARCHER> //This parameter ordering is unintuitive. But it makes it easier to call this function because SEARCHER is usually a complex parameter that you do not want to specify explicitly.
void _find_paths_with_backtracking(
	VertexDescriptor & start_node, //starting node for backtracking
	VertexDescriptor & end_node, //end node for backtracking
	Graph::NodeMap<bool> & subgraph_vertices, //out param: Used for marking nodes that lie on a backtracking path
	SEARCHER & searcher ) { //traceback information

	VertexDescriptor current_node = start_node;
	while( current_node != end_node && current_node != lemon::INVALID ) {
		subgraph_vertices[ current_node ] = true;
		current_node = searcher.predNode( current_node );
	}
}

template <typename WEIGHT_TYPE, class SEARCHER_FORWARD, class SEARCHER_BACKWARD>
void _coordinate_backtracking(
	Graph & graph,
	SEARCHER_FORWARD & searcher_forward,
	SEARCHER_BACKWARD & searcher_backward,
	const unsigned int max_distance, //maximum distanace between KO_node and other nodes
	VertexDescriptor & KO_node, //starting node for both BFS searches
	Graph::NodeMap<bool> & marked_vertices, //Nodes that are fixed
	const Graph::ArcMap<WEIGHT_TYPE> & arc_weights, //weights of arcs
	Graph::NodeMap<bool> & subgraph_vertices //out-param, marking the nodes that will be part of the resulting subgraph containing the nodes on the paths

	) {
	//Backtracking for each initially marked node.
	FilterGraph marked_vertices_graph( graph, marked_vertices ); //for selecting which nodes to loop over
	for( FilterGraph::NodeIt vertex( marked_vertices_graph );
	     vertex != lemon::INVALID;
	     ++vertex ) {

		//mark initial node
		subgraph_vertices[ vertex ] = true;

		 //traceback for forward search
		asm( "#first check" ); //for inspecting generated assembly code
		if( searcher_forward.dist( vertex ) <= max_distance ) {
			_find_paths_with_backtracking(
				vertex,
				KO_node,
				subgraph_vertices,
				searcher_forward );
		 }

		//traceback for backward search
		asm( "#second check" ); //for inspecting generated assembly code
		if( searcher_backward.dist( vertex ) <= max_distance ) {
			_find_paths_with_backtracking(
				vertex,
				KO_node,
				subgraph_vertices,
				searcher_backward );
		}
	}
}

template <typename WEIGHT_TYPE>
void find_paths_with_bellman_ford(
	Graph & graph, //the original graph
	const unsigned int max_distance, //maximum distanace between KO_node and other nodes
	VertexDescriptor & KO_node, //starting node for both BFS searches
	Graph::NodeMap<bool> & marked_vertices, //Nodes that are fixed
	const Graph::ArcMap<WEIGHT_TYPE> & arc_weights, //weights of arcs
	Graph::NodeMap<bool> & subgraph_vertices //out-param, marking the nodes that will be part of the resulting subgraph containing the nodes on the paths
	) {

	//forward search
	lemon::BellmanFord<Graph,Graph::ArcMap<WEIGHT_TYPE>> bellford( graph, arc_weights );
	Graph::NodeMap<WEIGHT_TYPE> distMap( graph );
	bellford.distMap( distMap );
	bellford.run( KO_node );

	//backward search
	ReverseGraph graph_rev( graph ); //create a view with swapped arc directions
	lemon::BellmanFord<ReverseGraph,Graph::ArcMap<WEIGHT_TYPE>> bellford_rev( graph_rev, arc_weights );
	ReverseGraph::NodeMap<WEIGHT_TYPE> distMap_rev( graph );
	bellford_rev.distMap( distMap_rev );
	bellford_rev.run( KO_node );

	_coordinate_backtracking<WEIGHT_TYPE>(
		graph,
		bellford,
		bellford_rev,
		max_distance,
		KO_node,
		marked_vertices,
		arc_weights,
		subgraph_vertices
	);
}

template <typename WEIGHT_TYPE>
void find_paths_with_dijkstra(
	Graph & graph, //the original graph
	const unsigned int max_distance, //maximum distanace between KO_node and other nodes
	VertexDescriptor & KO_node, //starting node for both BFS searches
	Graph::NodeMap<bool> & marked_vertices, //Nodes that are fixed
	const Graph::ArcMap<WEIGHT_TYPE> & arc_weights, //weights of arcs
	Graph::NodeMap<bool> & subgraph_vertices //out-param, marking the nodes that will be part of the resulting subgraph containing the nodes on the paths
	) {

	//forward search
	lemon::Dijkstra<Graph,Graph::ArcMap<WEIGHT_TYPE>> dijkstra( graph, arc_weights );
	Graph::NodeMap<WEIGHT_TYPE> distMap( graph );
	dijkstra.distMap( distMap );
	dijkstra.run( KO_node );

	//backward search
	ReverseGraph graph_rev( graph ); //create a view with swapped arc directions
	lemon::Dijkstra<ReverseGraph,Graph::ArcMap<WEIGHT_TYPE>> dijkstra_rev( graph_rev, arc_weights );
	ReverseGraph::NodeMap<WEIGHT_TYPE> distMap_rev( graph );
	dijkstra_rev.distMap( distMap_rev );
	dijkstra_rev.run( KO_node );

	_coordinate_backtracking<WEIGHT_TYPE>(
		graph,
		dijkstra,
		dijkstra_rev,
		max_distance,
		KO_node,
		marked_vertices,
		arc_weights,
		subgraph_vertices
	);
}

//generic path finding for paths with any number of linker nodes between KO and degs
//using the breadth-first-search (BFS) algorithm
template <typename WEIGHT_TYPE>
void find_paths_with_BFS(
	Graph & graph, //the original graph
	const unsigned int max_distance, //maximum distanace between KO_node and other nodes
	VertexDescriptor & KO_node, //starting node for both BFS searches
	Graph::NodeMap<bool> & marked_vertices, //Nodes that are fixed
	const Graph::ArcMap<WEIGHT_TYPE> & arc_weights, //weights of arcs
	Graph::NodeMap<bool> & subgraph_vertices //out-param, marking the nodes that will be part of the resulting subgraph containing the nodes on the paths
	) {
	
	//strategy: Do two BFS searches. Both starting from the KO node, but the second one will use a ReverseGraph that has it's arc directions flipped.
	//Then do backtracking from each DE node so that we capture downstream
	//and  feedback paths for the KO experiement.

	//Note that this function does a full BFS search.
	//But backtracking is limited in length

	//downstream search
	lemon::Bfs<Graph> bfs( graph );
	bfs.run( KO_node );

	//upstream search
	lemon::ReverseDigraph<Graph> graph_rev( graph ); //create a view with swapped arc directions
	lemon::Bfs<ReverseGraph> bfs_rev( graph_rev );
	bfs_rev.run( KO_node );

	_coordinate_backtracking<WEIGHT_TYPE>(
		graph,
		bfs,
		bfs_rev,
		max_distance,
		KO_node,
		marked_vertices,
		arc_weights,
		subgraph_vertices
	);
}


//path finding for paths with up to 1 linker node between two DE nodes
void mark_linker_nodes(
	Graph & graph, //the original graph
	Graph::NodeMap<bool> & marked_vertices, //Nodes that are fixed
	Graph::NodeMap<bool> & subgraph_vertices //out-param
	) {


	FilterGraph marked_subgraph( graph, marked_vertices );
	for( FilterGraph::NodeIt vertex( marked_subgraph );
	     vertex != lemon::INVALID;
	     ++vertex ) {

		subgraph_vertices[ vertex ] = true;

		//Now add all neighbors to the subgraph. But note: Not all of
		//them will be part of the final graph! Only neighbor-nodes that
		//connect an initially marked node with another initially marked
		//node will be kept in the end.
		for( Graph::OutArcIt arc( graph, vertex );
		     arc != lemon::INVALID;
		     ++arc ) {
			subgraph_vertices[ graph.target( arc ) ] = true;
		}
		for( Graph::InArcIt arc( graph, vertex );
		     arc != lemon::INVALID;
		     ++arc ) {
			subgraph_vertices[ graph.source( arc ) ] = true;
		}

	}

	std::vector<VertexDescriptor> vertices_to_remove;

	FilterGraph subgraph( graph, subgraph_vertices );

#ifdef DEBUG
	std::cout << "liberal subgraph has " << lemon::countNodes( subgraph )
		  << " vertices and " << lemon::countArcs( subgraph )
		  << " arcs" << std::endl;
#endif

	//remove all non-linker nodes
	for( FilterGraph::NodeIt vertex( subgraph );
	     vertex != lemon::INVALID;
	     ++vertex ) {
		if( marked_vertices[ vertex ] ) {
			continue; //pre-marked vertex. Will be part of the result set anyway
		}

		bool has_marked_downstream_vertex = false;
		bool has_marked_upstream_vertex = false;
		for( FilterGraph::OutArcIt arc( subgraph, vertex ); arc != lemon::INVALID; ++arc ) {
			if( marked_vertices[ subgraph.target( arc ) ] ) {
				has_marked_downstream_vertex = true;
				break;
			}
		}
		for( FilterGraph::InArcIt arc( subgraph, vertex ); arc != lemon::INVALID; ++arc ) {
			if( marked_vertices[ subgraph.source( arc ) ] ) {
				has_marked_upstream_vertex = true;
				break;
			}
		}
		if( !has_marked_downstream_vertex || !has_marked_upstream_vertex ) {
			vertices_to_remove.push_back( vertex ); //not a linker node. Prepare for removal
		}
	}

	//Mark non-linker nodes for removal
	for( auto it = vertices_to_remove.begin();
	     it != vertices_to_remove.end();
	     ++it ) {
		subgraph_vertices[ *it ] = false;
	}
}

using SearchAlgorithm = void (*)(
	Graph & graph, //the original graph
	const unsigned int max_distance, //maximum distanace between KO_node and other nodes
	VertexDescriptor & KO_node, //starting node for both BFS searches
	Graph::NodeMap<bool> & marked_vertices, //Nodes that are fixed
	const Graph::ArcMap<ArcWeight> & arc_weights, //weights of arcs
	Graph::NodeMap<bool> & subgraph_vertices //out-param, marking the nodes that will be part of the resulting subgraph containing the nodes on the paths
);

SearchAlgorithm get_search_function( std::string search_algo_name ) {
	if( search_algo_name == "bfs" ) return find_paths_with_BFS;
	else if( search_algo_name == "dijkstra" ) return find_paths_with_dijkstra;
	else if( search_algo_name == "bellford" ) return find_paths_with_bellman_ford;
	else return NULL;
}

int main( int argc, char ** argv ) {
	int args_counts = 0;
	const int network_file_position = ++args_counts;
	const int search_algorithm_position = ++args_counts;
	const int max_linker_nodes_position = ++args_counts;
	const int KO_parameter_position = ++args_counts;

	if( argc <= KO_parameter_position ) {
		std::cerr
			<< "Usage: knockout-deg-subtree pathway_commons_full_graph.tab search_algorithm max_linker_nodes KO [DE ...]" << std::endl
			<< "search_algorithm (only used when max_linker_nodes > 1): One of bfs, dijkstra, bellford" << std::endl;
		return 1;
	}

	unsigned int max_linker_nodes = std::atoi( argv[ max_linker_nodes_position ] ); //TODO error handling

	std::unordered_map<VertexName,VertexDescriptor> vertex_descriptors;

	Graph full_graph;

	Graph::NodeMap<VertexName> vertex_names( full_graph );
	GraphUtil::lemon::read_graph_from_file( argv[ network_file_position ],
			                        full_graph,
			                        vertex_names,
			                        vertex_descriptors ); //sets up full_graph and vertex_descriptors

#ifdef DEBUG
	std::cout << "Full graph has " << lemon::countNodes( full_graph )
		  << " vertices and " << lemon::countArcs( full_graph )
		  << " arcs" << std::endl;
#endif

	Graph::NodeMap<bool> marked_vertices( full_graph );
	for( int i = KO_parameter_position; i < argc; i++ ) {
		if( vertex_descriptors.find( argv[ i ] ) == vertex_descriptors.end() ) {
			if( i == KO_parameter_position ) {
				std::cerr << "Fatal: KO node " << argv[ i ] << " not found in graph" << std::endl;
				return 1;
			} else {
				std::cerr << "No vertex with name " << argv[ i ] << std::endl;
			}
		} else {
			VertexDescriptor command_line_vertex = vertex_descriptors[ argv[ i ] ];
			marked_vertices[ command_line_vertex ] = true;
		}
	}
	VertexDescriptor KO_node = vertex_descriptors[ argv[ KO_parameter_position ] ];

	Graph::NodeMap<bool> subgraph_vertices( full_graph, false );

	Graph::ArcMap<ArcWeight> arc_weights( full_graph, 1.0 ); //TODO implement reading weights from file

	FilterGraph * subgraph; //need to "declare" outside of switch's curly braces
	switch( max_linker_nodes ) {
		case 0:
			subgraph = new FilterGraph( full_graph, marked_vertices ); //forget about the subgraph_vertices and use the marked_vertices instead
			break;
		case 1:
			mark_linker_nodes( full_graph, marked_vertices, subgraph_vertices ); //this changes subgraph_vertices
			subgraph = new FilterGraph( full_graph, subgraph_vertices );
			break;
		default:
			SearchAlgorithm search_function = get_search_function( argv[ search_algorithm_position ] );
			if( search_function == NULL ) {
				std::cerr << "Incorrect algorithm name: " << argv[ search_algorithm_position ] << std::endl;
				return 2;
			}
			search_function(
				full_graph,
				max_linker_nodes + 1, //+1 because we need to convert to maximum distance
				KO_node,
				marked_vertices,
				arc_weights, //not used for BFS
				subgraph_vertices ); //this changes subgraph_vertices
			subgraph = new FilterGraph( full_graph, subgraph_vertices );
	}


#ifdef DEBUG
	std::cout << "subgraph has " << lemon::countNodes( subgraph )
		  << " vertices and " << lemon::countArcs( subgraph )
		  << " arcs" << std::endl;
#endif

	for( FilterGraph::ArcIt arc( *subgraph ); arc != lemon::INVALID; ++arc ) {
		std::cout
			<< vertex_names[ subgraph -> source( arc ) ]
			<< " -> "
			<< vertex_names[ subgraph -> target( arc ) ]
			<< std::endl;
	}

	delete subgraph;

	return 0;
}
