#include <iostream>
#include <unordered_map>
#include <fstream>
#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/breadth_first_search.hpp>
#include <boost/graph/properties.hpp> //provides graph_bundle

#include "timetaker.h"
#include "graph-util-boost.h"
#include "boost-benchmark.h"

int main( int argc, char ** argv ) {
	std::unordered_map<VertexName,VertexDescriptor> vertex_descriptors;
	std::unordered_map<VertexDescriptor, VertexDescriptor> backtracking; //vertex -> predecessor

	Graph graph;
	if( argc < 3 ) {
		std::cerr << "Please provide a graph file and a knockout" << std::endl;
		return 1;
	}
	read_graph_from_file( argv[ 1 ], graph, vertex_descriptors ); //initializes graph and vertex_descriptors

	//first build up a BFS search tree starting from the KO to all vertices
	//in the network. We can use this search tree for all DE genes in one
	//direction.

	GraphUtil::boost::CustomBFSVisitor vis( argv[ 2 ], vertex_descriptors, &backtracking );
	VertexDescriptor ko_vertex = vertex_descriptors[ argv[ 2 ] ];
	breadth_first_search(
		graph,
		ko_vertex,
		visitor( vis ) );

	for( int i = 3; i < argc; i++ ) {
		GraphUtil::boost::print_bfs_backtrace( graph, vis, argv[ i ], vertex_descriptors, backtracking ); // KO -> ... -> DE
	}

	//Now the more expensive part. We need a new search tree for each DE
	//gene.
	for( int i = 3; i < argc; i++ ) {
		backtracking.clear(); //vertex -> predecessor
		GraphUtil::boost::CustomBFSVisitor vis( argv[ 2 ], vertex_descriptors, &backtracking );
		VertexDescriptor de_vertex = vertex_descriptors[ argv[ i ] ];
		breadth_first_search(
			graph,
			de_vertex,
			visitor( vis ) );
		GraphUtil::boost::print_bfs_backtrace( graph, vis, argv[ 2 ], vertex_descriptors, backtracking );
	}
	return EXIT_SUCCESS;
}
