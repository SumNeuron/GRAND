#ifndef GRAPH_UTIL_BOOST_H
#define GRAPH_UTIL_BOOST_H

#include <string>
#include <fstream>
#include <unordered_map>

#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/breadth_first_search.hpp>

namespace GraphUtil {

	namespace boost {
		using VertexName = std::string;
		struct Vertex {
			VertexName name;
		};
		struct Edge {};
		struct GraphProperties {};
		using Graph = ::boost::adjacency_list<
		              ::boost::vecS, //OutEdgeList
		              ::boost::vecS, //VertexList
		              ::boost::directedS, //Directed
		              Vertex, //VertexProperty
		              Edge, //EdgeProperty
		              GraphProperties //GraphProperty
		>;
		using VertexDescriptor = Graph::vertex_descriptor;
		using EdgeWeight = std::string;
		using VertexMapping = std::unordered_map<VertexName, VertexDescriptor>;
		using Backtracking = std::unordered_map<VertexDescriptor, VertexDescriptor>;

		void read_graph_from_file( std::string filename, Graph & out_graph, VertexMapping & vertex_descriptors );

		//TODO move implementation into cpp file
		class CustomBFSVisitor : public ::boost::default_bfs_visitor {
			VertexDescriptor _vertex_to_find;
			Backtracking * _backtracking;
			public:
				bool done_with_search;
				//CustomBFSVisitor( VertexName vertex_to_find, VertexMapping & vertex_descriptors, Backtracking * backtracking );
				//void tree_edge( Edge edge, const Graph & graph );

				CustomBFSVisitor( VertexName vertex_to_find, VertexMapping & vertex_descriptors, Backtracking * backtracking ) {
					_vertex_to_find = vertex_descriptors[ vertex_to_find ]; //TODO summarize as a single constructor parameter
					done_with_search = false;
					_backtracking = backtracking;
				}



				template <typename Edge, typename Graph> //TODO this seems like a troublemaker
				void tree_edge( Edge edge, const Graph & graph ) {
					VertexDescriptor target = ::boost::target( edge, graph );
					VertexDescriptor source = ::boost::source( edge, graph );
					(*_backtracking)[ target ] = source;

					if( target == _vertex_to_find ) {
						done_with_search = true;
					}
				}
		};

		VertexDescriptor get_vertex_index_with_name( Graph & graph, std::string & vertex_name );
		void read_graph_from_file( std::string filename, Graph & out_graph );

		void print_bfs_backtrace( Graph & graph, CustomBFSVisitor & vis, VertexName vertex_name, VertexMapping & vertex_descriptors, Backtracking & backtracking );

	}
}
#endif
