#include <iostream>

#include "graph-util-boost.h"

namespace GraphUtil {
	namespace boost {
		//TODO this is duplicate stuff from the graph-util.h see how to
		//remove it
		using VertexName = std::string;
		using Graph = ::boost::adjacency_list<
		              ::boost::vecS, //OutEdgeList
		              ::boost::vecS, //VertexList
		              ::boost::directedS, //Directed
		              Vertex, //VertexProperty
		              Edge, //EdgeProperty
		              GraphProperties //GraphProperty
		>;
		using VertexDescriptor = Graph::vertex_descriptor;
		using EdgeWeight = std::string;
		using VertexMapping = std::unordered_map<VertexName, VertexDescriptor>;
		using Backtracking = std::unordered_map<VertexDescriptor, VertexDescriptor>;
		//end of duplicated stuff

		using CustomBFSVisitor = ::GraphUtil::boost::CustomBFSVisitor;

		VertexDescriptor get_vertex_index_with_name( Graph & graph, std::string & vertex_name, VertexMapping & vertex_descriptors ) {
			if( vertex_descriptors.count( vertex_name ) == 0 ) {
				VertexDescriptor new_vertex = add_vertex( graph );
				//graph_bundle.vertex_names[ vertex_name ] = new_vertex;
				vertex_descriptors[ vertex_name ] = new_vertex;
				graph[ new_vertex ].name = vertex_name; //store for human-readable backtracking
				return new_vertex;
			}
			return vertex_descriptors[ vertex_name ];
		}

		void read_graph_from_file( std::string filename, Graph & out_graph, VertexMapping & vertex_descriptors ) {
			std::string line;
			std::ifstream file( filename );
			if ( !file.is_open() ) {
				perror( "Function Graph::Graph| error while opening file" );
			}
			bool expect_file_to_end = false;
			while( !file.eof() ) {
				assert( !expect_file_to_end ); //if this is false, then the file format is bad or there is a bug in the program

				VertexName vertex_A_name, vertex_B_name;
				EdgeWeight edge_weight;

				file >> vertex_A_name;
				file >> vertex_B_name;
				file >> edge_weight;

				if( vertex_A_name == "" || vertex_B_name == "" ) {
					expect_file_to_end = true;
					continue; //most likely there was an unecessary newline before the EOF
						  //if that should not be the case, the assert
						  //at the beginning of this loop will strike
				}

				VertexDescriptor vertex_a = get_vertex_index_with_name( out_graph, vertex_A_name, vertex_descriptors );
				VertexDescriptor vertex_b = get_vertex_index_with_name( out_graph, vertex_B_name, vertex_descriptors );

				add_edge( vertex_a, vertex_b, out_graph );
			}

			if ( file.bad() ) {
				std::perror( "Function read_graph_from_file| error while reading file" );
			}
		}

		void print_bfs_backtrace( Graph & graph, CustomBFSVisitor & vis, VertexName vertex_name, VertexMapping & vertex_descriptors, Backtracking & backtracking ) {
			
			//auto graph_bundle = graph[ boost::graph_bundle ];
			VertexDescriptor vertex = vertex_descriptors[ vertex_name ];
			while( backtracking.count( vertex ) ) {
				std::cout << graph[ vertex ].name << " <- ";
				vertex = backtracking[ vertex ];
			}
			std::cout << graph[ vertex ].name << std::endl;
		}
	}
}
