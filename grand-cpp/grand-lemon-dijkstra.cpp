#include <string>
#include <fstream>
#include <cassert>
#include <cstdlib>
#include <unordered_map>

#include <lemon/list_graph.h>
#include <lemon/dijkstra.h>

#include "grand-lemon-dijkstra.h"

using Graph = lemon::ListDigraph;
using NodeMap = Graph::NodeMap<std::string>;
using VertexName = std::string;
using EdgeWeight = unsigned int;
using EdgeWeightMap = Graph::ArcMap<int>;
using VertexDescriptor = Graph::Node;


//evil global variable
std::unordered_map<VertexName,VertexDescriptor> vertex_descriptors;

VertexDescriptor get_vertex_index_with_name( Graph & graph, std::string & vertex_name ) {
	//auto graph_bundle = graph[ boost::graph_bundle ];
	if( vertex_descriptors.count( vertex_name ) == 0 ) {
		VertexDescriptor new_vertex = graph.addNode();
		vertex_descriptors[ vertex_name ] = new_vertex; //assign to global variable
		return new_vertex;
	}
	return vertex_descriptors[ vertex_name ];
}

void read_graph_from_file( std::string filename, Graph & out_graph, NodeMap & vertex_names, EdgeWeightMap & edge_weights  ) {
        std::string line;
        std::ifstream file( filename );
        if ( !file.is_open() ) {
                perror( "Function Graph::Graph| error while opening file" );
        }
        bool expect_file_to_end = false;
	//auto graph_bundle = out_graph[ boost::graph_bundle ]; //TODO remove this line
        while( !file.eof() ) {
                assert( !expect_file_to_end ); //if this is false, then the file format is bad or there is a bug in the program

                VertexName vertex_A_name, vertex_B_name;
                EdgeWeight edge_weight;

                file >> vertex_A_name;
                file >> vertex_B_name;
                file >> edge_weight;

                if( vertex_A_name == "" || vertex_B_name == "" ) {
                        expect_file_to_end = true;
                        continue; //most likely there was an unecessary newline before the EOF
                                  //if that should not be the case, the assert
                                  //at the beginning of this loop will strike
                }
		VertexDescriptor vertex_a = get_vertex_index_with_name( out_graph, vertex_A_name );
		VertexDescriptor vertex_b = get_vertex_index_with_name( out_graph, vertex_B_name );

		//this might be a bit wasteful because it might overwrite
		//existing entries sometimes
		vertex_names[ vertex_a ] = vertex_A_name; //store for human-readable backtracking
		vertex_names[ vertex_b ] = vertex_B_name; //store for human-readable backtracking

		Graph::Arc edge = out_graph.addArc( vertex_a, vertex_b );
		edge_weights[ edge ] = edge_weight;

        }

        if( file.bad() ) {
		std::perror( "Function read_graph_from_file| error while reading file" );
        }
}


int main( int argc, char ** argv ) {

	if( argc < 3 ) {
		std::cerr << "[USAGE] grand-lemon-dijkstra reference-network-file KO-gene [DE-gene ...]" << std::endl;
		return 1;
	}
	std::string reference_network_filename = argv[ 1 ];
	std::string KO = argv[ 2 ];

	Graph g;
	NodeMap vertex_names( g );
	EdgeWeightMap edge_weights( g );
	read_graph_from_file( reference_network_filename, g, vertex_names, edge_weights );

	lemon::Dijkstra<Graph> dijkstra( g, edge_weights );

	Graph::Node KO_node = vertex_descriptors[ KO ];
	std::string vertex_name = vertex_names[ KO_node ];
	dijkstra.run( KO_node );

	for( int i = 3; i < argc; i++ ) {
		Graph::Node DE_node = vertex_descriptors[ argv[ 3 ] ];
		if( dijkstra.reached( DE_node ) ) { //Make sure there is a known path from start to finish
			for( Graph::Node node = DE_node; node != KO_node; node = dijkstra.predNode( node ) ) {
				std::cout << vertex_names[ node ] << " ";
			}
			std::cout << vertex_names[ KO_node ] << std::endl; //also print KO
		} else {
			std::cout << "Vertex " << argv[ 3 ] << " is not reachable." << std::endl;
		}
	}
}
