#!/usr/bin/env python

from sys import argv, exit, stderr
from random import shuffle


def slurp_gene_list( filename ):
    with open( filename ) as f:
        return list( x.rstrip() for x in f.readlines() )


if __name__ == '__main__':
    if len( argv ) < 4:
        std.write( "Usage: generate_bfs_queries.py <vertexlist> <number-of-lists> <length-of-generated-lists>" )
        exit( 1 )
    filename = argv[ 1 ]
    number_of_lists = int( argv[ 2 ] )
    list_lengths = int( argv[ 3 ] )

    genelist = slurp_gene_list( filename )
    for i in range( number_of_lists ):
        shuffle( genelist )
        print( ' '.join( genelist[ :list_lengths ] ) )
