#include <iostream>

#include "timetaker.h"

PointInTime walltime() {
	return std::chrono::system_clock::now();
}

TimeTaker::TimeTaker() {
	this -> now = walltime();
	this -> max_diff = std::chrono::seconds( 0 );
	this -> max_diff_label = "";
	this -> verbose = true;
}

void TimeTaker::time_passed( std::string label ) {
	PointInTime right_now = walltime();
	Duration diff = std::chrono::duration_cast<std::chrono::milliseconds>( right_now - this -> now );
	if( this -> verbose ) {
		std::cout << "[TIME] <" << label << "> " << diff.count() << " milliseconds" << std::endl;
	}
	this -> now = right_now;
	if( this -> max_diff < diff ) {
		this -> max_diff = diff;
		this -> max_diff_label = label;
	}
}

void TimeTaker::print_max_diff() {
	std::cout << "[TIME] Maximum time span: <" << this -> max_diff_label << "> with " << this -> max_diff.count() << " milliseconds " << std::endl;
}
