#ifndef GRAND_H
#define GRAND_H

#include <map>
#include <unordered_map>
#include <set>
#include <unordered_set>

class Graph; //declaration
class Vertex; //declaration

/* type aliases */
using VertexName = std::string;
using VertexSet = std::map<VertexName,Vertex>;
using EdgeWeight = std::string;
using VertexNameSet = std::unordered_set<VertexName>;

typedef double ( *DistanceFunction )( Graph*, Vertex*, Vertex* );

enum SearchDirection {
        Predecessors = 0,
        Successors = 1
};

class Graph {
        VertexSet vs; //vertex name to Vertex object mapping

        Vertex * getClosestVertexForAStar( VertexNameSet * open_set,
                                           std::unordered_map<VertexName,double> * f_scores );

        public:
        Graph( char * filename );
        Vertex * get_vertex( VertexName name );
        void list_edges();
        void a_star_depth_lock(
                std::vector<VertexName> * out_result,
                VertexName start,
                VertexName stop,
                DistanceFunction real_distance,
                DistanceFunction heuristic_distance,
                int max_steps_from_start,
                SearchDirection direction );
};

class Vertex {
        public:
        VertexName name;
        std::unordered_map<VertexName,EdgeWeight> successors;
        std::unordered_map<VertexName,EdgeWeight> predecessors;

        public:
        Vertex(); //default constructor
        Vertex( VertexName name );
};

void determine_final_a_star_path( std::vector<VertexName> * out_result, std::unordered_map<VertexName,VertexName> * backtracking, VertexName stop );

#endif
