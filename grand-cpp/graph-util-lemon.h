#ifndef GRAPH_UTIL_LEMON_H
#define GRAPH_UTIL_LEMON_H

#include <string>
#include <fstream>
#include <unordered_map>

#include <lemon/list_graph.h>

namespace GraphUtil {
	namespace lemon {
		using Graph = ::lemon::ListDigraph;
		using NodeMap = Graph::NodeMap<std::string>;
		using VertexName = std::string;
		using EdgeWeight = std::string;
		using VertexDescriptor = Graph::Node;
		using VertexMapping = std::unordered_map<VertexName, VertexDescriptor>;

		void read_graph_from_file(
			std::string filename,
			Graph & out_graph,
			Graph::NodeMap<std::string> & vertex_names,
			VertexMapping & vertex_descriptors
		);
	}
}

#endif
