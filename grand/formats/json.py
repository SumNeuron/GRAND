import os, sys, operator, json
from ..helpers.log import log
from ..helpers.warn import warn







def dump_json(graph, verbosity:bool=True) -> None:
    jsdump = json.dumps({"vertexSet": graph.vertex_set, 'edgeSet': graph.edge_set, 'layering': graph.layering})
    return jsdump


def dump_json_to(graph, json_file:str, directory:str='./', verbosity:bool=True) -> None:
    kill = False
    # make sure user passed an argument to graphml_file
    #kill = warn(graphml_file, None, test=operator.eq, fatal=True, verbosity=verbosity)
    if kill:
        return

    with open(os.path.join(directory, json_file), 'w') as f:
        f.write(dump_json(graph))





# def load_json(json_file:str, directory:str='./', verbosity:bool=True) -> None:
#     with open(os.path.join(directory, json_file), 'r') as f:
#


def edge_set_to_json(graph):
    # p_edges and q_edges are from the definition of the efficient sugiyama
    # algorithm; namely, p edges are those from source to first dummy vertex
    # and q_edges are those from last dummy vertex to target, for edges with a span
    # more than 2. For edges (from the original graph) with a span >> 1, there
    # may be many dummy vertices injected to ensure a proper layering.
    # This is fine for drawing a static graph, however how should all these intermediate
    # vertices be handles when the graph is interactive (users can draw vertices).
    # For this purpose, we will add a key to each edge, "path", which will contain
    # a list of vertex keys of where the path goes. This path's length will be
    # bounded [2, 4], where 2 is an edge that require no dummy vertices,
    # 3, are edges with an s edge (p,q edge shares a vertex) and 4 are the vertices
    # from the p and q edges.

    # This allows for those who wish to make the vertices drag-able, capable of
    # of allowing users to move these intermediate nodes as well (and updating the)
    # edge path.

    properly_layered_flag = True

    pq_edges = graph.__get_pq_paths__()
    r_edges = graph.__get_r_paths__()

    es = {}

    if not pq_edges and not r_edges:
        r_edges = [[e['source'], e['target']] for e in graph.edge_set.values()]

    print(pq_edges)
    print(r_edges)
    for i, edge in enumerate(pq_edges):
        k = edge[0]+'_'+edge[-1]
        es[k] = graph.removed_edges[k]
        es[k]['path'] = edge

        if es[k]['reverse']:
            pass


    for i, edge in enumerate(r_edges):
        k = edge[0]+'_'+edge[-1]
        es[k] = graph.edge_set[k]
        es[k]['path'] = edge
    return es
