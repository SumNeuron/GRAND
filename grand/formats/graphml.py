import os, sys, operator, io
# sys.path.insert(0, "../helpers")

from ..helpers.log import log
from ..helpers.warn import warn



def parse_graphml(text, verbosity:bool=True) -> dict :
    """
    Read in graphml file.

    Keyword arguments:
    directory -- the directory in which the file resides.
    graphml_file -- the graphml file which should be parsed.
    verbosity -- whether or not to print log statements.
    """
    vertices = []
    edges = []
    if type(text) == str:
        lines = text.split('\n')
    if isinstance(text, io.IOBase): # test if text is a file io stream
        lines = text.readlines()

    for line in lines:
        if 'node' in line:
            not_needed, node = line.split('id=')
            node, not_needed = node.split('/')
            vertices.append(node)

        if '<edge' in line:
            not_needed, source = line.split('source=')
            source, target = source.split(' target=')
            target, not_needed = target.split('/')
            edges.append([source, target])

    vertices = [v.replace('"', '') for v in vertices]
    edges = [[s.replace('"', ''), t.replace('"', '')] for (s, t) in edges]

    return {'vertices': vertices, 'edges': edges}

def parse_graphml_from(directory:str='./', graphml_file:str=None, verbosity:bool=True) -> dict :
    """
    Read in graphml file.

    Keyword arguments:
    directory -- the directory in which the file resides.
    graphml_file -- the graphml file which should be parsed.
    verbosity -- whether or not to print log statements.
    """
    kill = False

    # make sure user passed an argument to graphml_file
    kill = warn(graphml_file, None, test=operator.eq, fatal=True, verbosity=verbosity)
    if kill:
        return

    # make sure extension is graphml
    ext = graphml_file.split('.')[-1]
    kill = warn(ext, 'graphml', test=operator.ne, fatal=True, verbosity=verbosity)
    if kill:
        return


    vertices = []
    edges = []
    with open(os.path.join(directory, graphml_file), 'r') as gf:
        return parse_graphml(gf)



def dump_graphml(graph, verbosity:bool=True) -> None :
    """
    Dump out graphml file.

    Keyword arguments:
    graph -- a graph object
    directory -- the directory in which the file resides.
    graphml_file -- the graphml file which should be created.
    verbosity -- whether or not to print log statements.
    """

    vertices = list(graph.vertex_set.keys())
    edges = [[e['source'], e['target']]  for e in graph.edge_set.values()] # needs to come from graph

    file_string = ""
    file_string += '<?xml version="1.0" encoding="UTF-8"?>\n'
    file_string += '<!DOCTYPE graphml SYSTEM "http://www.graphdrawing.org/dtds/graphml.dtd">\n'
    file_string += '<graphml>\n'
    file_string += '<graph id="GRAND">\n'
    for vertex in vertices:
        file_string += '<node id="{v}"/>\n'.format(v=vertex)
    for source, target in edges:
        file_string += '<edge id="{s}_{t}" source="{s}" target="{t}"/>\n'.format(s=source, t=target)

    file_string += '</graph>\n'
    file_string += '</graphml>'

    return file_string


def dump_graphml_to(graph, graphml_file:str,  directory:str='./', verbosity:bool=True) -> None :
    """
    Dump out graphml file.

    Keyword arguments:
    graph -- a graph object
    directory -- the directory in which the file resides.
    graphml_file -- the graphml file which should be created.
    verbosity -- whether or not to print log statements.
    """
    kill = False

    # make sure user passed an argument to graphml_file
    kill = warn(graphml_file, None, test=operator.eq, fatal=True, verbosity=verbosity)
    if kill:
        return

    file_string = dump_graphml(graph)

    with open(os.path.join(directory, graphml_file), 'w') as gf:
        gf.write(file_string)
