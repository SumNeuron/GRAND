class Helpers:
    def __set_spans__(self, layering):
        for edge_name, edge_values in self.items():
            edge_values.set_span(layering=layering)

    def __downward_edges_of_layer__(self, layer):
        return self.select(**{'source': lambda source: source in layer})

    def __upward_edges_of_layer__(self, layer):
        return self.select(**{'target': lambda target: target in layer})

    def __make_layering_keys__(self):
        for key in ['span', 'directed', "reversed"]:
            self.add_subkey(key)

    def __make_dummy_keys__(self):
        self.add_subkey('class', 'regular')

    def __make_crossing_type_keys__(self):
        self.add_subkey('crossing_types', {'type_0': None, 'type_1': None, 'type_2': None})

    def __remove_dummy_keys__(self):
        for key in ['class','crossing_types']:
            self.delete_subkey(key)

    def __remove_layering_keys__(self):
        for key in ['span', 'directed', "reversed"]:
            self.delete_subkey(key)

    def __remove_crossing_types_keys__(self):
        self.delete_subkey('crossing_types')

    def s_edges(self):
        return self.select(**{'class': 's_edge'})

    def p_edges(self):
        return self.select(**{'class': 'p_edge'})

    def q_edges(self):
        return self.select(**{'class': 'q_edge'})

    def reverse(self, edge_key):

        edge = self[edge_key]
        source_key, target_key = edge.vw()

        new_edge_key = target_key + "_" + source_key
        i = 1
        while new_edge_key in self.keys():
            i += 1
            new_edge_key = target_key + i * "_" + source_key

        edge['source'] = target_key
        edge['target'] = source_key
        edge['reversed'] = not edge.reversed()
        self[new_edge_key] = edge
        del self[edge_key]
        return new_edge_key

    def reverse(self, edge_key):
        edge = self[edge_key]
        source_key, target_key = edge.vw()

        edge.reverse()

        new_edge_key = target_key + "_" + source_key
        i = 1
        while new_edge_key in self.keys():
            i += 1
            new_edge_key = target_key + i * "_" + source_key

        self[new_edge_key] = edge
        del self[edge_key]
        return new_edge_key
