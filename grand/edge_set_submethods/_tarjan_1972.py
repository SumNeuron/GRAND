# https://epubs.siam.org/doi/10.1137/0201010
class Tarjan1972:
    def __edges_between_strongly_connected_components__(self, strongly_connected_components):
        edges_between = []
        for edge_key, edge_values in self.items():
            u, w = edge_values.vw()
            for current, component_1 in enumerate(strongly_connected_components):
                for component_2 in strongly_connected_components[current:]:

                    if component_1 == component_2:
                        continue

                    if (u in component_1 and w in component_2) or (w in component_1 and u in component_2):
                        edges_between.append(edge_key)

        return edges_between

    def __feedback_set_from_linear_ordering__(self, vertex_ordering):  # set of edges whose reversal makes a DAG
        edges_to_reverse = []
        for edge_key, edge in self.items():
            u, w = edge.vw()
            if vertex_ordering.index(u) > vertex_ordering.index(w):
                edges_to_reverse.append(edge_key)
        return edges_to_reverse
