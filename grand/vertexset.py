from .ond import OrderedNestedDictionary
from .vertex import Vertex
import copy
import math
import queue


# Horizontal coordinate assignment
from .vertex_set_submethods._brandes_kopf_2002 import BrandesKopf2002
# Strongly connected components
from .vertex_set_submethods._tarjan_1972 import Tarjan1972
# Layering
from .vertex_set_submethods._healy_nikolov import HealyNikolov
# Ordering for layering
from .vertex_set_submethods._eades_lin_smyth_1989 import EadesLinSmyth1989
# Layering post-processing: root demotion
from .vertex_set_submethods._magruder_bonn_2017 import MagruderBonn2017
# Layering post-processing: promote node
from .vertex_set_submethods._nikolov_tarassov_2005 import NikolovTarassov2005
# Ordering for layering
from .vertex_set_submethods._magruder_layering import MagruderMagnitude
# Layering
from .vertex_set_submethods._depth_first_search_layering import DFSLayering
# Helpers
from .vertex_set_submethods._helpers import Helpers
# Misc. layeing helpers
from .vertex_set_submethods._layering_helpers import LayeringHelpers

class VertexSet(
        OrderedNestedDictionary,
        BrandesKopf2002, Tarjan1972, HealyNikolov,
        EadesLinSmyth1989, MagruderBonn2017, NikolovTarassov2005, DFSLayering,
        MagruderMagnitude, LayeringHelpers, Helpers
    ):
    def __init__(self, template_keys=['predecessors', 'successors'], *args, **kwargs):

        super(VertexSet, self).__init__(template_keys=template_keys, *args, **kwargs)


    def __setitem__(self, key, item):
        if type(item) == type(Vertex()):
            super(VertexSet, self).__setitem__(key, item)
        else:
            return
        super(VertexSet, self).__setitem__(key, item)


    def neighborhood(self, vertex, scope=0, which='all'):
         predecessors = self[vertex]['predecessors']
         successors = self[vertex]['successors']

         neighbors = predecessors + successors

         last_index = 0
         while scope:
             for current_vertex in neighbors[last_index:]:
                 current_predecessors = self[current_vertex]['predecessors']
                 current_successors = self[current_vertex]['successors']

                 predecessors += current_predecessors
                 successors += current_successors

                 neighbors += current_predecessors
                 neighbors += current_successors
                 last_index += 1

         neighborhood = list(set(neighbors))

         if which == 'predecessors':
             neighborhood = list(set(predecessors))
         elif which == 'successors':
             neighborhood = list(set(successors))


         return neighborhood
