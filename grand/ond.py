from collections import OrderedDict
from operator import itemgetter, attrgetter
from statistics import mean, median, median_high, median_low
from copy import deepcopy

"""
OrderedNestedDictionary is a class meant for handling an ordered dictionary of dictionaries i.e. for each key-value
pair in OrderedNestedDictionary, the value is a dictionary (or derived therefrom). Furthermore, this class is built
with the presumption that every value of the contained key-value pairs shares a similar 'template' (mentioned below)
In discourse of the OrderedNestedDictionary class one refers to the keys and values of the outer-most dictionary as
such, for each nesting one prepends sub prior to the term key and value. e.g. an OrderedNestedDictionary class
contains key-value pairs such that every value is itself a dictionary. Given the value (a dictionary) of a key-value
pair belonging to the OrderedNestedDictionary class, that value consists of sub-key -- sub-value pairs. We say that
the values of the OrderedNestedDictionary share a template if and only if, every value in the key-value pairs, share
the same sub-keys.

{ 'key_1': # value_1 is a template-dictionary
           {'sub_key_1': sub_value_1,
            'sub_key_2': sub_value_2,
            'sub_key_3': sub_value_3},
  'key_2': # value_2
           {'sub_key_1': sub_value_1,
            'sub_key_2': sub_value_2,
            'sub_key_3': sub_value_3}
  'key_3': # value_3
           {'sub_key_1': sub_value_1,
            'sub_key_2': sub_value_2,
            'sub_key_3': sub_value_3}
} # end of OrderedNestedDictionary
NOTE: although 'sub_key_1', 'sub_key_2', and 'sub_key_3' are equivalent for value_1, value_2, and value_3 (hence
defining a template) the corresponding sub_values may change per instance

"""
from .ond_submethods._override import Override
from .ond_submethods._statistic_helpers import StatisticsHelpers
from .ond_submethods._useful_properties import UsefulProperties
from .ond_submethods._gather_select import GatherSelect

class OrderedNestedDictionary(
        OrderedDict, Override, StatisticsHelpers, UsefulProperties,
        GatherSelect
    ):
    _template_keys = None


    def __init__(self, template_keys=None, *args, **kwds):
        self._template_keys = template_keys
        cleaned = {k: v for k, v in kwds.items() if isinstance(v, (dict,))}
        super(OrderedNestedDictionary, self).__init__(*args, **cleaned)

    def at(self, i:int):
        """
        at(i) retrieves the key of element in the ith position in the ordered dictionary.
        """
        return self.__getitem__(list(self.keys())[i])

    def sort_by(self, subkey, reverse=False):
        # self = OrderedNestedDictionary(sorted(iter(self.items()), key=lambda item: item[1][subkey], reverse=reverse))
        # print(self)
        post_sort = sorted(iter(self.items()), key=lambda item: item[1][subkey], reverse=reverse)
        for key, value in post_sort:
            super(OrderedNestedDictionary, self).move_to_end(key)

        return None


    # add_/delete_subkey adds / deletes the subkey into every value in the OrderedNestedDictionary
    def add_subkey(self, subkey, default=None):
        for key, value in self.items():
            if subkey not in value.keys():
                value[subkey] = deepcopy(default)

    def delete_subkey(self, subkey):
        for key, value in self.items():
            try:
                del value[subkey]
            except KeyError:
                pass

    def name_values(self):
        """
        name_values generates a key, 'name' for each value, such that the corresponding value of the new key is the key
        of the value (thereby allowing one, given only the value, to know the associated key).
        """
        for key, value in self.items():
            value['name'] = key
