class DFSLayering:
    # Modified DFS layering algorithm
    def __depth_first_search_layering__(self):  # my own algorithm :)
        roots = self.select(**{'predecessors': lambda x: x == []})
        # roots = self.select('predecessors', selector=lambda x: x == [])


        ## If only predecessor is self, then vertex  is also a root
        for vertex in set(self).difference(set(roots)):
            if [vertex] == self[vertex]['predecessors']:
                roots.append(vertex)

        roots = sorted(roots, key=lambda r: len(self[r]['successors']) == 0)  # empty roots at end

        for root in roots:
            self[root]['layer'] = 0
            self.__depth_first_search_layering_recursion__(root)

        for vertex in self:  ## stop pure cycles / catch untouched nodes, a < -- > b disconnected from rest of graph
            if self[vertex]['layer'] == None:
                self.__depth_first_search_layering_recursion__(vertex)

        number_of_layers = 0
        for vertex in self:
            if self[vertex]['layer'] > number_of_layers:
                number_of_layers = self[vertex]['layer']
        return number_of_layers + 1

    def __depth_first_search_layering_recursion__(self, vertex):

        if not self[vertex]['layer'] == 0:
            predecessors = set(self[vertex]['predecessors']).difference({vertex})
            try:
                self[vertex]['layer'] = max(self.gather(predecessors).select(result='value',
                                                                             **{'layer': lambda x: type(x) == int})) + 1
            except ValueError:
                self[vertex]['layer'] = 0

        targets = self[vertex]['successors']
        # print(targets)
        targets = sorted(targets, key=lambda t: len(self[t]['successors']))
        targets.reverse()
        for target in targets:
            preds = self[target]['predecessors']
            if self.gather(preds).select(result='value', **{'layer': lambda x: x == None}) != []:
                continue

            if self[target]['layer'] == None:
                self.__depth_first_search_layering_recursion__(target)
