import copy

class NikolovTarassov2005:
    # https://www.sciencedirect.com/science/article/pii/S0166218X05003100
    def __nikolov_tarassov__(self):
        layering = self.__assemble_layering_from_subkeys__()
        layering_back_up = layering
        promotions = 1
        while promotions:
            promotions = 0
            for vertex in self:
                if self[vertex].in_degree():
                    dummy_diff = self.__nikolov_tarassov_promote_node__(vertex)
                    if dummy_diff < 0:
                        promotions += 1
                        layering_back_up = copy.deepcopy(self.__assemble_layering_from_subkeys__())

                    else:
                        layering = copy.deepcopy(layering_back_up)
                        self.__update_subkeys_from_layering__(layering)
        self.__update_subkeys_from_layering__(layering)


    def __nikolov_tarassov_promote_node__(self, vertex):
        dummy_difference = 0
        predecessors = self[vertex]['predecessors']
        for predecessor in predecessors:
            if self[predecessor]['layer'] == self[vertex]['layer'] - 1:
                dummy_difference = dummy_difference + self.__nikolov_tarassov_promote_node__(predecessor)

        self[vertex]['layer'] -= 1
        if self[vertex]['layer'] < 0:
            diff = abs(self[vertex]['layer'])  # + 1
            for v in self:
                self[v]['layer'] += diff

        dummy_difference = dummy_difference - self[vertex].in_degree() + self[vertex].out_degree()
        return dummy_difference
