class EadesLinSmyth1989:
    """
    P. Eades, X. Lin, and W. F. Smyth.  Heuristics for the feedback arc set
    problem.  Technical Report 1, Curtin University of Technology, School of
    Computing Science, Perth, Australia, 1989.
    """
    # Linear Ordering algorithms for cycle breaking
    def __eades_et_al__(self):
        # page 414 of HDA Chapter 13.2.1 Heuristics Based on Vertex Ordering
        ordering = []
        for vertex_name, vertex_values in self.items():
            ordering.append([vertex_name, vertex_values.out_degree()])
        ordering = sorted(ordering, key=lambda x: x[1])
        ordering = [x[0] for x in ordering]
        return ordering
