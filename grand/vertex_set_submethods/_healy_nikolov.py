class HealyNikolov:
    # Modified implementation of Healy and Nikolov's longest path layering algorithm
    def __healy_nikolov__(self):
        """
        See page 420-1 of Chapter 13: Hierarchical Graph Drawing by Healy and Nikolov.
        Modified to switch layering (e.g. layer 0 is up top instead of at the bottom)
        U = vertices assigned to a layer
        Z = all vertices assigned to a layer above the current
        V = Vertex Set
        v is selected from v in V \ U
        """
        U = set()
        Z = set()
        V = set(self.keys())

        current_layer = 0
        moved_layer = False
        while U != V:

            unassigned = V - U
            choices = set(filter(lambda v: set(self[v]['predecessors']).issubset(Z), unassigned))

            if not choices and moved_layer:
                choices = sorted(unassigned, key=lambda v: len(self[v]['predecessors']))
            if choices:
                selected = choices.pop()
                self[selected]['layer'] = current_layer
                U.add(selected)
                moved_layer = False

            else:
                current_layer += 1
                Z = Z.union(U)
                moved_layer = True

        return current_layer + 1
