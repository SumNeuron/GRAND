class MagruderMagnitude:
    def __magruder_magnitute__(self):
        ordering = list(self.keys())
        # ordering = sorted(ordering, key=lambda x: x["in_degree"] + x["out_degree"])
        # ordering.reverse()
        move_made = True
        while move_made:
            move_made = False
            for i in range(len(ordering) - 1):
                if self[ordering[i]].in_degree() > self[ordering[i + 1]].in_degree() \
                        and self[ordering[i]].in_degree() + self[ordering[i]].out_degree() == \
                                        self[ordering[i + 1]].in_degree() + self[ordering[i + 1]].out_degree():
                    temp = ordering[i + 1]
                    ordering[i + 1] = ordering[i]
                    ordering[i] = temp
                    move_made = True
        return ordering
