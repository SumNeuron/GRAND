class Tarjan1972:
    # Tarjan Strongly Connected Components
    def __tarjan_strongly_connected_components__(self):
        """
        VertexSet.__tarjan_strongly_connected_components__

        This determines the strongly connected components (SCCs) following the efficient travesal strategy laid out by
        Tarjan in 1986.

        """
        self.__make_tarjan_keys__()

        tarjan_index = 0
        stack = []
        strongly_connected_components = []
        for vertex_name, vertex_values in self.items():
            if vertex_values['tarjan_index'] == None:
                vertex_values.__strong_connect__(self, tarjan_index, stack, strongly_connected_components)


        # extract vertex keys from SCCs
        strongly_connected_components_names = [[] for i in range(len(strongly_connected_components))]

        for scc, strongly_connected_component in enumerate(strongly_connected_components):
            for component in strongly_connected_component:
                strongly_connected_components_names[scc].append(component['name'])

        self.__remove_tarjan_keys__()
        return strongly_connected_components_names

    def __remove_tarjan_keys__(self):
        tarjan_keys = ['tarjan_index', 'on_tarjan_stack', 'tarjan_low_link', 'name']
        for key in tarjan_keys:
            self.delete_subkey(key)

    def __make_tarjan_keys__(self):
        tarjan_keys = ['tarjan_index', 'on_tarjan_stack', 'tarjan_low_link']
        for key in tarjan_keys:
            self.add_subkey(key)
        self.name_values()
