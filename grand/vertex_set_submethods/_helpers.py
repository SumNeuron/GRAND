class Helpers:
    # Retrieve subset of vertices
    def p_vertices(self):
        return self.select(result='key', selection='every', **{'class': lambda c: c == 'p_vertex'})

    def q_vertices(self):
        return self.select(result='key', selection='every', **{'class': lambda c: c == 'q_vertex'})

    def r_vertices(self):
        return self.select(result='key', selection='every', **{'class': lambda c: c == 'r_vertex'})

    def dummy_vertices(self):
        dummies = []
        for id, vertex in self.items():
            if vertex.dummy_q():
                dummies.append(id)
        return dummies

    # Make preset keys
    def __make_layering_keys__(self):
        layering_keys = ['layer', 'position_in_layer']
        for key in set(layering_keys) - {'class'}:
            self.add_subkey(key, default=None)

    def __make_dummy_keys__(self):
        self.add_subkey('class', default='regular')

    def __make_edge_degree_keys__(self):
        edge_keys = ['in_degree', 'out_degree']
        for key in edge_keys:
            self.add_subkey(key, default=0)





    def __make_coordinate_keys__(self):
        self.add_subkey("coordinates", {"x": None, "y": None})

    # Remove preset keys
    def __remove_layering_keys__(self):
        layering_keys = ['layer', 'position_in_layer', 'class']
        for key in layering_keys:
            self.delete_subkey(key)

    def __remove_dummy_keys__(self):
        self.delete_subkey('class')

    def __remove_edge_degree_keys__(self):
        edge_keys = ['in_degree', 'out_degree']
        for key in edge_keys:
            self.delete_subkey(key)


    def __remove_coordinate_keys__(self):
        self.delete_subkey("coordinates")

    # Retrieve relative information from
    def __inner_segments_of__(self, vertex_key, which="all"):
        # layer = above, below or any
        incident_edges = []
        if self[vertex_key]['class'] == 'regular':
            pass
        else:
            if which == 'all':
                predecessors = self[vertex_key]['predecessors']
                successors = self[vertex_key]['successors']
                for predecessor in predecessors:
                    if self[predecessor]['class'] == 'regular':
                        continue
                    incident_edges.append(predecessor + "_" + vertex_key)

                for successor in successors:
                    if self[successor]['class'] == 'regular':
                        continue
                    incident_edges.append(vertex_key + "_" + successor)

            elif which == "predecessors":
                predecessors = self[vertex_key]['predecessors']
                for predecessor in predecessors:
                    if self[predecessor]['class'] == 'regular':
                        continue
                    incident_edges.append(predecessor + "_" + vertex_key)

            elif which == "successors":
                successors = self[vertex_key]['successors']
                for successor in successors:
                    if self[successor]['class'] == 'regular':
                        continue
                    incident_edges.append(vertex_key + "_" + successor)

        return incident_edges


    
