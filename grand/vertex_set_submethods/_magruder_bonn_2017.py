import queue

# http://jgaa.info/accepted/2017/MagruderBonn2017.21.6.pdf
class MagruderBonn2017:
    def __root_demotion__(self):
        leaves = self.select(**{'successors': lambda x: x == []})
        seen = []
        for leaf in leaves:
            self.__root_demotion_recursion__(leaf, seen)
        # for vertex in self.keys():
        #     self.__root_demotion_recursion__(vertex)

    def __root_demotion_recursion__(self, vertex, seen):
        if vertex in seen:
            return
        successors = self[vertex]['successors']
        if successors:
            max_layer = min(self.gather(successors).select(result='value',
                                                           **{'layer': lambda x: x is not None}))
            if max_layer - self[vertex]['layer'] > 1:
                self[vertex]['layer'] = max_layer - 1

        seen.append(vertex)
        for source in self[vertex]['predecessors']:
            if self[vertex]['layer'] - self[source]['layer'] > 1:
                self.__root_demotion_recursion__(source, seen)


    # def __root_demotion_recursion__(self, vertex):
    #     successors = self[vertex]['successors']
    #     if successors:
    #         max_layer = min(self.gather(successors).select(result='value',
    #                                                        **{'layer': lambda x: x is not None}))
    #         if max_layer - self[vertex]['layer'] > 1:
    #             self[vertex]['layer'] = max_layer - 1
    #
    #     for source in self[vertex]['predecessors']:
    #         if self[vertex]['layer'] - self[source]['layer'] > 1:
    #             self.__root_demotion_recursion__(source)


    def __root_demotion__(self):
        lowest_layer = self.max_of('layer', result='subvalue')
        bottom_vertices = self.select(**{'layer': lambda l: l == lowest_layer})
        for v in bottom_vertices:
            self.__root_demotion_bfs__(v)

    def __root_demotion_bfs__(self, v):
        q = queue.Queue()
        q.put(v)
        seen = [v]

        while not q.empty():
            current = q.get()
            successors = self[current]['successors']
            if successors:
                max_layer = min(self.gather(successors).select(result='value', **{'layer': lambda x: x is not None}))

                print(current, self[current]['layer'], max_layer)

                if max_layer - self[current]['layer'] > 1:
                    self[current]['layer'] = max_layer - 1

            pred = self[current]['predecessors']
            pred = sorted(pred, key=lambda p: self[p]['layer'])
            # reversed(pred)

            [(q.put(p), seen.append(p)) for p in pred if p not in seen]



    def __root_demotion__(self):
        vertices = sorted(list(self.keys()), key=lambda v: self[v]['layer'])

        while vertices:
            vertex = vertices.pop()
            successors = self[vertex]['successors']
            if successors:
                max_layer = min(self.gather(successors).select(result='value',
                                                               **{'layer': lambda x: x is not None}))
                if max_layer - self[vertex]['layer'] > 1:
                    self[vertex]['layer'] = max_layer - 1


    def __leaf_promotion__(self):
        # leaves = self.select(**{'successors': lambda x: x == []})
        # for leaf in leaves:
        #     self.__depth_first_search_layering_demotion_recursion__(leaf)
        for vertex in self.keys():
            self.__leaf_promotion_recursion__(vertex)

    def __leaf_promotion_recursion__(self, vertex):
        predecessors = self[vertex]['predecessors']

        if predecessors:
            lowest_layer = max(self.gather(predecessors).select(result='value',
                                                           **{'layer': lambda x: x is not None}))

            if self[vertex]['layer'] - lowest_layer > 1:
                self[vertex]['layer'] = lowest_layer + 1

        for target in self[vertex]['successors']:
            # print(vertex, source)
            if self[target]['layer'] - self[vertex]['layer'] > 1:
                self.__leaf_promotion_recursion__(target)
