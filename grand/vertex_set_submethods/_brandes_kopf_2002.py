class BrandesKopf2002:
    def __make_brandes_kopf_keys__(self):
        for key, vertex in self.items():
            vertex['brandes_kopf'] = {'upper_left': {'root': key,
                                                     'align': key,
                                                     'sink': key,
                                                     'shift': float('inf'),
                                                     'coordinates': {'x': None, 'y': None}},
                                      'upper_right':{'root': key,
                                                     'align': key,
                                                     'sink': key,
                                                     'shift': float('inf'),
                                                     'coordinates': {'x': None, 'y': None}},

                                      'lower_left': {'root': key,
                                                     'align': key,
                                                     'sink': key,
                                                     'shift': float('inf'),
                                                     'coordinates': {'x': None, 'y': None}},

                                      'lower_right':{'root': key,
                                                     'align': key,
                                                     'sink': key,
                                                     'shift': float('inf'),
                                                     'coordinates': {'x': None, 'y': None}}
                                      }
    def __remove_brandes_kopf_keys__(self):
        self.delete_subkey('brandes_kopf')

    def __place_block_upper_right__(self, a, bk, ct, delta, direction, l, layering, g, pos, r, root, sf, sk, x, xy, v):
        if self[v][bk][g][xy][x] is None:
            self[v][bk][g][xy][x] = 0

            w = v
            w_is_v = False
            while not w_is_v:

                pos_w = self[w][pos]
                if pos_w < len(layering[self[w][l]]) - 1:

                    successor_of_w = layering[self[w][l]][pos_w + 1]
                    u = self[successor_of_w][bk][g][r]

                    self.__place_block__(u, delta, direction, layering)

                    sink_of_v = self[v][bk][g][sk]
                    sink_of_u = self[u][bk][g][sk]

                    x_v = self[v][bk][g][xy][x]
                    x_u = self[u][bk][g][xy][x]


                    if sink_of_v == v:
                        self[v][bk][g][sk] = sink_of_u
                        sink_of_v = self[v][bk][g][sk]

                    if sink_of_v != sink_of_u:
                        shift_sink_u = self[sink_of_u][bk][g][sf]
                        self[sink_of_u][bk][g][sf] = max([shift_sink_u, x_v - x_u - delta])

                    else:
                        self[v][bk][g][xy][x] = min([x_v, x_u - delta])


                w = self[w][bk][g][a]
                w_is_v = w == v

    def __place_block_upper_left__(self, a, bk, ct, delta, direction, l, layering, g, pos, r, root, sf, sk, x, xy, v):
        if self[v][bk][g][xy][x] is None:
            self[v][bk][g][xy][x] = 0
            w = v

            w_is_v = False
            while not w_is_v:

                pos_w = self[w][pos]
                if pos_w > 0:

                    predecessor_of_w = layering[self[w][l]][pos_w - 1]
                    u = self[predecessor_of_w][bk][g][r]

                    self.__place_block__(u, delta, direction, layering)

                    sink_of_v = self[v][bk][g][sk]
                    sink_of_u = self[u][bk][g][sk]

                    x_v = self[v][bk][g][xy][x]
                    x_u = self[u][bk][g][xy][x]

                    if sink_of_v == v:
                        self[v][bk][g][sk] = sink_of_u
                        sink_of_v = self[v][bk][g][sk]

                    if sink_of_v != sink_of_u:
                        shift_sink_u = self[sink_of_u][bk][g][sf]
                        self[sink_of_u][bk][g][sf] = min([shift_sink_u, x_v - x_u - delta])

                    else:
                        self[v][bk][g][xy][x] = max([x_v, x_u + delta])

                w = self[w][bk][g][a]
                w_is_v = w == v

    def __place_block_lower_right__(self, a, bk, ct, delta, direction, l, layering, g, pos, r, root, sf, sk, x, xy, v):
        if self[v][bk][g][xy][x] is None:
            self[v][bk][g][xy][x] = 0
            w = v
            w_is_v = False
            while not w_is_v:

                pos_w = self[w][pos]
                if pos_w < len(layering[self[w][l]]) - 1:

                    successor_of_w = layering[self[w][l]][pos_w + 1]
                    u = self[successor_of_w][bk][g][r]

                    self.__place_block__(u, delta, direction, layering)

                    sink_of_v = self[v][bk][g][sk]
                    sink_of_u = self[u][bk][g][sk]

                    x_v = self[v][bk][g][xy][x]
                    x_u = self[u][bk][g][xy][x]


                    if sink_of_v == v:
                        self[v][bk][g][sk] = sink_of_u
                        sink_of_v = self[v][bk][g][sk]

                    if sink_of_v != sink_of_u:
                        shift_sink_u = self[sink_of_u][bk][g][sf]
                        self[sink_of_u][bk][g][sf] = max([shift_sink_u, x_v - x_u - delta])

                    else:
                        self[v][bk][g][xy][x] = min([x_v, x_u - delta])


                w = self[w][bk][g][a]
                w_is_v = w == v

    def __place_block_lower_left__(self, a, bk, ct, delta, direction, l, layering, g, pos, r, root, sf, sk, x, xy, v):
        if self[v][bk][g][xy][x] is None:
            self[v][bk][g][xy][x] = 0
            w = v

            w_is_v = False
            while not w_is_v:

                pos_w = self[w][pos]
                if pos_w > 0:

                    predecessor_of_w = layering[self[w][l]][pos_w - 1]
                    u = self[predecessor_of_w][bk][g][r]

                    self.__place_block__(u, delta, direction, layering)

                    sink_of_v = self[v][bk][g][sk]
                    sink_of_u = self[u][bk][g][sk]

                    x_v = self[v][bk][g][xy][x]
                    x_u = self[u][bk][g][xy][x]

                    if sink_of_v == v:
                        self[v][bk][g][sk] = sink_of_u
                        sink_of_v = self[v][bk][g][sk]

                    if sink_of_v != sink_of_u:
                        shift_sink_u = self[sink_of_u][bk][g][sf]
                        self[sink_of_u][bk][g][sf] = min([shift_sink_u, x_v - x_u - delta])

                    else:
                        self[v][bk][g][xy][x] = max([x_v, x_u + delta])

                w = self[w][bk][g][a]
                w_is_v = w == v

    # Brandes Köpf
    def __place_block__(self, vertex_key, delta, direction='upper_left', layering=None):

        align = 'align'
        brandes_kopf = 'brandes_kopf'
        coordinates = 'coordinates'
        crossing_types = 'crossing_types'
        layer = 'layer'
        position_in_layer = 'position_in_layer'
        root = 'root'
        shift = 'shift'
        sink = 'sink'

        a = align
        bk = brandes_kopf
        ct = crossing_types
        g = direction  # g for greedy_direction
        l = layer
        pos = position_in_layer
        r = root
        sf = shift
        sk = sink
        x = 'x'
        xy = coordinates

        v = vertex_key

        if direction == 'upper_left':
            self.__place_block_upper_left__(a, bk, ct, delta, direction, l, layering, g, pos, r, root, sf, sk, x, xy, v)

        if direction == 'upper_right':
            self.__place_block_upper_left__(a, bk, ct, delta, direction, l, layering, g, pos, r, root, sf, sk, x, xy, v)

        if direction == 'lower_left':
            self.__place_block_lower_left__(a, bk, ct, delta, direction, l, layering, g, pos, r, root, sf, sk, x, xy, v)

        if direction == 'lower_right':
            self.__place_block_lower_right__(a, bk, ct, delta, direction, l, layering, g, pos, r, root, sf, sk, x, xy, v)
