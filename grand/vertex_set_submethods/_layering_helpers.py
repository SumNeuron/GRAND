class LayeringHelpers:
    # Make a layering of the Vertex Set
    def __assemble_layering_from_subkeys__(self, number_of_layers=None):
        min_layer = self.min_of('layer', result='subvalue')
        max_layer = self.max_of('layer', result='subvalue')

        if number_of_layers is None:
            number_of_layers = max_layer - min_layer + 1


        layering = [[] for i in range(number_of_layers)]
        for id, vertex in self.items():
            layering[vertex['layer'] - min_layer].append(id)
            vertex['position_in_layer'] = layering[vertex['layer'] - min_layer].index(id)

        layering = [layer for layer in layering if layer != []]
        self.__update_subkeys_from_layering__(layering)
        return layering

    def __update_subkeys_from_layering__(self, layering):
        for i, layer in enumerate(layering):
            for j, vertex in enumerate(layer):
                self[vertex]['layer'] = i
                self[vertex]['position_in_layer'] = j

    def __make_layering__(self, method='healy_nikolov', implementation=''):
        _methods = ['healy_nikolov', 'depth_first_search']
        _implementations = ['promotion', 'demotion', 'promote_layering']
        self.__make_layering_keys__()

        if method == 'healy_nikolov':
            number_of_layers = self.__healy_nikolov__()

        if method == 'depth_first_search':
            number_of_layers = self.__depth_first_search_layering__()

        implementations = implementation.split(' ')
        for imp in implementations:
            if imp == 'promote_layering':
                self.__nikolov_tarassov__()
                number_of_layers = self.max_of('layer', result='subvalue') + 1

            if imp == 'demotion':
                self.__root_demotion__()

            if imp == 'promotion':
                self.__leaf_promotion__()

        layering = self.__assemble_layering_from_subkeys__()
        return layering
