class Edge(dict):
    _keys = ['source', 'target']

    def __init__(self, **kwargs):
        for key in set(self._keys) - set(kwargs.keys()):
            if key == 'source' or key == 'target':
                self[key] = ''

        for key in kwargs:
            self[key] = kwargs[key]

    def calculate_span(self, layering):
        i, j = None, None
        for layer_index, layer in enumerate(layering):
            if self["source"] in layer:
                i = layer_index
            if self["target"] in layer:
                j = layer_index
        return j - i

    def get_class(self):
        try:
            return self['class']
        except KeyError:
            return ''

    def classed(self, class_str, which=None):
        cur_class = self.get_class()
        if which is None:
            return class_str in cur_class
        if which:
            # add class to string
            if self.classed(class_str):
                # class in string, continue
                return
            else:
                # add class to string
                self['class'] = cur_class + ' ' + class_str
                return
        else:
            # remove class from class string
            if self.classed(class_str):
                # class is there remove it
                self['class'] = cur_class.replace(class_str, '')
            else:
                # class is not there, who cares
                return



    def set_span(self, span=None, layering=None):
        if span is None and layering is not None:
            span = self.calculate_span(layering)
        if type(span) == int:
            self["span"] = span

    def vw(self):
        return self['source'], self['target']

    def reverse(self):
        v, w = self.vw()
        try:
            if self["reversed"]:
                self["reversed"] = False
                self["source"] = w
                self["target"] = v
            else:
                self["reversed"] = True
                self["source"] = w
                self["target"] = v
        except KeyError:
            self["reversed"] = True
            self["source"] = w
            self["target"] = v

        c = self.get_class()
        p_flag = self.classed('p_edge')
        q_flag = self.classed('q_edge')
        # print(self)

        self.classed('p_edge', False)
        self.classed('q_edge', False)
        if p_flag:
            self.classed('q_edge', True)
            # print(self)
        if q_flag:
            self.classed('p_edge', True)
            # print(self)



    def span(self):
        try:
            return self['span']
        except KeyError:
            print('KeyError: calling Edge has no key "span". None returned')
            return None

    def needs_dummy_q(self):
        if self.span() > 1:
            return True
        else:
            return False

    def dummy_q(self):
        try:
            markers = ['r_', 's_', 'p_', 'q_', 'dummy']
            for marker in markers:
                if marker in self['class']:
                    return True
            return False
        except KeyError:
            print('KeyError: calling Edge has no key "class". None returned.')
            return None

    def p_edge(self):
        try:
            markers = ['p_']
            for marker in markers:
                if marker in self['class']:
                    return True
            return False
        except KeyError:
            print('KeyError: calling Edge has no key "class". None returned.')
            return None

    def reversed(self):
        try:
            return self['reversed']
        except KeyError:
            return False
            # print('KeyError: calling Edge has no key "reversed". None returned.')

    def connection_type(self):
        try:
            return self['connection_type']
        except KeyError:
            return None
            print('KeyError: calling Edge has no key "connection_type". None returned.')
