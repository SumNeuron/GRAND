from .ond import OrderedNestedDictionary
from .edge import Edge

from .edge_set_submethods._tarjan_1972 import Tarjan1972
from .edge_set_submethods._helpers import Helpers

class EdgeSet(OrderedNestedDictionary, Tarjan1972, Helpers):
    def __init__(self, template_keys=['source', 'target'],*args, **kwargs):
        super(EdgeSet, self).__init__(template_keys=template_keys, *args, **kwargs)

    def __setitem__(self, key, item):
        if type(item) == type(Edge()):
            super(EdgeSet, self).__setitem__(key, item)
        else:
            return
        super(EdgeSet, self).__setitem__(key, item)
