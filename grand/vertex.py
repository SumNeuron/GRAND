from .vertex_submethods._tarjan_1972 import Tarjan1972

class Vertex(dict, Tarjan1972):
    _keys = ['predecessors', 'successors']

    def __init__(self, **kwargs):

        for key in set(self._keys) - set(kwargs.keys()):
            if key == 'predecessors' or key == 'successors':
                self[key] = []

        for key in kwargs:
            self[key] = kwargs[key]


    def has_key_q(self, key:str) -> bool:
        """
        Tests if vertex contains the given key.
        """
        if key in self:
            return True
        return False

    def __incident_edge_reversed__(self, other_vertex:str, others_relation:str='source') -> None:
        """
        Moves the given vertex from the 'source' list to 'target' list and vis versa.

        Keyword arguments:
        other_vertex -- the name of the vertex to move.
        others_relation -- either 'source' or 'target' specify from where to move <other_vertex> to.
        """


        if self.has_key_q('in_degree'):
            if others_relation == "target":
                self["out_degree"] -= 1
                self["in_degree"] += 1
                self["successors"].pop(self["successors"].index(str(other_vertex)))
                self["predecessors"].append(str(other_vertex))

            elif others_relation == "source":
                self["out_degree"] += 1
                self["in_degree"] -= 1
                self["predecessors"].pop(self["predecessors"].index(str(other_vertex)))
                self["successors"].append(str(other_vertex))
        else:
            if others_relation == "target":
                self["successors"].pop(self["successors"].index(str(other_vertex)))
                self["predecessors"].append(str(other_vertex))

            elif others_relation == "source":
                self["predecessors"].pop(self["predecessors"].index(str(other_vertex)))
                self["successors"].append(str(other_vertex))


    def out_degree(self):
        return len(self['successors'])

    def in_degree(self):
        return len(self['predecessors'])

    def degree(self):
        return len(self['successors']) + len(self['predecessors'])

    def successors(self):
        return self['successors']

    def predecessors(self):
        return self['predecessors']

    def dummy_q(self):
        try:
            markers = ['r_', 's_', 'p_', 'dummy']
            for marker in markers:
                if marker in self['class']:
                    return True
            return False
        except KeyError:
            # print('KeyError: calling Vertex has no key "class". None returned.')
            return False

    def __edges__(self, id):
        edges = []
        for successor in self['successors']:
            edges += [id + '_' + successor]
        for predecessor in self['predecessors']:
            edges += [predecessor + '_' + id]
        return edges
