from itertools import chain, groupby

def tally(array):
    items_in_list = [key for key, iterator_thing in groupby(array)]
    tally = []
    for item in items_in_list:
        counter = 0
        for other in items_in_list:
            if item == other:
                counter += 1
        tally.append([item, counter])
    return tally
