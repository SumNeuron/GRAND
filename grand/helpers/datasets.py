import os, operator, sys

from .warn import warn
from .log import log
from ..formats import graphml


def not_contains(b, a) -> bool:
    """
    Helper function for 'not operator.contains'.

    Keyword arguments:
    b -- object to test if a is in
    a -- thing to see if it is contained in b
    """
    return not operator.contains(b, a)


def datasets_available(dataset_directory:str=os.path.join(os.path.dirname(os.path.realpath(__file__)).split('graph')[0],"datasets"), verbosity=True) -> list:
    """
    Lists directories (datasets) in dataset_directory.
    """
    dirs = [listing for listing in os.listdir(dataset_directory) if os.path.isdir(os.path.join(dataset_directory, listing))]
    log(status='AVAILABLE DATA',message=dirs, verbosity=verbosity)
    return dirs




def load_dataset(dataset:str, dataset_directory:str=os.path.join(os.path.dirname(os.path.realpath(__file__)).split('graph')[0],"datasets"), verbosity=True) -> dict:
    """
    Reads in all graph files from the given dataset into a dictionary.

    Keyword arguments:
    dataset -- which dataset to use
    dataset_directory -- where _all_ datasets are stored. Default '../grand/datasets'
    verbosity -- whether to print log statements.
    """
    kill = False

    available_datasets = datasets_available()

    kill = warn(available_datasets, dataset, not_contains, fatal=True, verbosity=verbosity)
    if kill:
        return

    data_dir = os.path.join(dataset_directory, dataset)

    graphs = {}
    files = [ f for f in os.listdir(data_dir) if '.log' not in f ]
    for f in files:
        graphs[f] = graphml.parse_graphml_from(directory=data_dir, graphml_file=f, verbosity=verbosity)

    return graphs

def load_all_datasets(dataset_directory:str=os.path.join(os.path.dirname(os.path.realpath(__file__)).split('graph')[0],"datasets"), verbosity=True) -> dict:
    kill = False

    available_datasets = datasets_available()

    data_dirs = [os.path.join(dataset_directory, dataset) for dataset in available_datasets]
    graphs = {}

    for i, data_dir in enumerate(data_dirs):
        log(status='LOAD',message='graphs from the {dataset} dataset.'.format(dataset=available_datasets[i]), verbosity=verbosity)
        files = [ f for f in os.listdir(data_dir) if '.log' not in f ]
        for f in files:
            graphs[f] = graphml.parse_graphml_from(directory=data_dir, graphml_file=f, verbosity=verbosity)

    return graphs
