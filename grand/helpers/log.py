def log(status:str='log',message:str='message',verbosity:bool=True)->None:
    print('[{status}]\t{message}'.format(status=status,message=message)) if verbosity else None
