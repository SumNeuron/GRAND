from .log import log
import operator
def warn(a, b, test, fatal:bool=False, verbosity:bool=True)->bool:
    """
    Tests argument for condition, logging if specified.

    Keyword arguments:
    a -- object to be tested
    b -- object to test a against
    test -- function which returns boolean
    fatal -- if thing == shouldNotBe returns true to let function terminate
    verbosity -- whether to print statements
    """
    kill = False
    result = test(a, b)
    if result:
        stat = 'FATAL' if fatal else 'WARN'
        mess = "a:{a}, b:{b} passed test {test}.".format(a=a, b=b, test=test)
        log(status=stat, message=mess)
        kill = True if fatal else False
    return kill
