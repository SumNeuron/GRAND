class Override:
    def __str__(self):
        instance_str = ""
        for k, v in self.items():
            instance_str += str(k) + '\n'
            for sk, sv in v.items():
                instance_str += '\t{}:\t{}\n'.format(str(sk), str(sv))
        return instance_str

    def __add__(self, other):
        # new dictionary
        new = {k: v for k, v in list(self.items())}

        # for each key, value in the other
        for key, value in list(other.items()):
            # if key is not already there, add
            if key not in list(new.keys()):
                new[key] = value
            # else, add new keys
            else:
                # marker to signify duplicate
                marker='*'
                # the number of duplicates of this key
                count = 0

                key_base = key
                # if key is already a duplicate
                if marker in key:
                    # split at the marker
                    split = key.split(marker)
                    last = split[-1]
                    rest = split[0:-1]
                    # update count by what follows after marker
                    if last.isdigit():
                        count = int(last)
                    else:
                        # last is not a number, so not a duplicate by our conventions
                        rest.append(last)
                    # merge the key back together
                    key_base = marker.join(rest)

                new_key = key_base + marker + str(count)

                # while new key exists in combined ONDs
                while new_key in list(new.keys()):
                    # increase count
                    count += 1
                    # add the marker and count
                    new_key = key_base + marker + str(count)

                new[new_key] = value
        return self.__class__(template_keys=self._template_keys, **new)


    def __setitem__(self, key, item):
        """
        Tests that the item being set has all of the template keys in its dictionary.

        Keyword arguments:
        key -- key of item to be set
        item -- item (dictionary) which is to be set to key
        """
        if self._template_keys is not None:
            if type(self._template_keys) == list:
                if all([template_key in list(item.keys()) for template_key in self._template_keys]):
                    super(self.__class__, self).__setitem__(key, item)
                else:
                    return
            elif type(self._template_keys) == dict:
                all_true_flag = True
                for key_index, (template_key, template_value_type) in enumerate(self._template_keys.items()):
                    if template_key in list(item.keys()):
                        if type(item[template_key]) != template_value_type:
                            all_true_flag = False
                            break
                    else:
                        all_true_flag = False
                        break
                if all_true_flag:
                    super(self.__class__, self).__setitem__(key, item)
                else:
                    return

        super(self.__class__, self).__setitem__(key, item)
