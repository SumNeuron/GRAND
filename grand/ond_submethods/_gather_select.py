class GatherSelect:
    """
    gather and select work similarly to reap and sow operations. select can be used to obtain the keys which have
    values (which are dictionaries) that have various sub-key -- sub-value pairs matching specified criteria. gather
    takes a list of such keys and makes a new instance of OrderedNestedDictionary containing just those passed keys.
    Together they provide a convenient way to filter via encapsulation e.g.

    OrderedNestedDictionary.gather(
        OrderedNestedDictionary.select(
                                           result='key',
                                           selection='every',
                                           **{'sub_key_1':lambda l: l == condition}
                                        )
                                    )
    """
    def gather(self, keys):
        """
        gather(keys) takes an array of keys and creates a new instance of OrderedNestedDictionary consisting of only
        those keys found in the calling instance.
        """
        collected = self.__class__()
        for key in keys:
            try:
                collected[key] = self[key]
            except KeyError:
                None
        return collected

    def select(self, result='key', selection='every', **kwargs):
        """
        select(result:selection:**kwargs) filters the calling OrderedNestedDictionary according to which values match
        the conditions specified by the **kwargs.

        :param result: can be either 'key', 'value' or 'key_value'. result dictates whether to return the key, the
        value, or both of those elements which satisfied the conditions specified by **kwargs.

        :param selection: can be either 'every' or 'any'. Works as a local logical AND or logical OR. If more than one
        condition is given in **kwargs will return the element (as specified by result) if 'every' or 'any' of the
        passed conditions are meet.

        :param kwargs: an arbitrary number of selectors. The form should be of sub-key--selector where sub-key is part
        of the template (e.g. a sub-key found in every value of the calling OrderedNestedDictionary). The selector
        should be a lambda function.
        """

        # to handle multi-keys currently testing for if the selector is actually a list
        # future / better imlpementations may have a set of tuples, or two lists one for args and one for selecetors
        # or 'descriptor': ('key', selector)



        selected = []
        for key, value in self.items():
            keep = False
            for subkey, selector in kwargs.items():
                if type(selector) is list:
                    for subselector in selector:
                        try:
                            if not subselector(value[subkey]):
                                if selection == 'every':
                                    keep = False
                                    break
                                elif selection == 'any':
                                    pass
                            else:
                                keep = True
                        except KeyError:
                            pass
                else:
                    try:
                        if not selector(value[subkey]):
                            if selection == 'every':
                                keep = False
                                break
                            elif selection == 'any':
                                pass
                        else:
                            keep = True
                    except KeyError:
                        pass
            if keep:
                if result == 'key':
                    selected.append(key)
                elif result == 'value':
                    selected.append(value[subkey])
                elif result == 'key_value':
                    selected.append((key, value[subkey]))

        return selected
