from statistics import mean, median, median_high, median_low

class StatisticsHelpers:
    """
    the following x_of functions are not given descriptions as they as general description serves them all. Given a
    sub-key in the template for all values of the OrderedNestedDictionary, x_of applies whatever function x is to the
    sub-value of each value's sub-key.
    """

    def min_of(self, subkey, result='key'):
        key, value = min(iter(self.items()), key=lambda item: item[1][subkey])
        subvalue = value[subkey]
        if result == 'key':
            return key
        elif result == 'value':
            return value
        elif result == 'subvalue':
            return subvalue
        elif result == 'key_value':
            return key, value
        elif result == 'key_subvalue':
            return key, subvalue
        return None


    def max_of(self, subkey, result='key'):
        key, value = max(iter(self.items()), key=lambda item: item[1][subkey])
        subvalue = value[subkey]
        if result == 'key':
            return key
        elif result == 'value':
            return value
        elif result == 'subvalue':
            return subvalue
        elif result == 'key_value':
            return key, value
        elif result == 'key_subvalue':
            return key, subvalue
        return None

    def mean_of(self, subkey):
        return mean([value[subkey] for value in self.values()])

    def median_of(self, subkey):
        return median([value[subkey] for value in self.values()])

    def median_low_of(self, subkey, result='key'):
        values = [value[subkey] for value in self.values()]
        subvalue = median_low(values)
        index = values.index(subvalue)
        key = list(self.keys())[index]
        value = self.at(index)
        if result == 'subvalue':
            return subvalue
        elif result == 'value':
            return value
        elif result == 'key':
            return key
        elif result == 'key_subvalue':
            return key, subvalue
        elif result == 'key_value':
            return key, value
        return None


    def median_high_of(self, subkey, result='key'):
        values = [value[subkey] for value in self.values()]
        subvalue = median_high(values)
        index = values.index(subvalue)
        key = list(self.keys())[index]
        value = self.at(index)
        if result == 'subvalue':
            return subvalue
        elif result == 'value':
            return value
        elif result == 'key':
            return key
        elif result == 'key_subvalue':
            return key, subvalue
        elif result == 'key_value':
            return key, value
        return None
