class UsefulProperties:
    # first, last, most, and rest are core functions lifted from the Wolfram Language. They may prove useful for
    # some computation across dictionary elements since they are ordered.

    @property
    def last(self):
        """
        last() returns the last key in the ordered dictionary.
        """
        return self.at(-1)

    @property
    def first(self):
        """
        first() returns the first key in the ordered dictionary
        """
        return self.at(0)

    @property
    def most(self):
        """
        most() returns all but the last key in the ordered dictionary
        """
        most = [(key, value) for key, value in list(self.items())[0:-1]]
        return most

    @property
    def rest(self):
        """
        rest() returns all but the first key in the ordered dictionary
        """
        rest = [(key, value) for key, value in list(self.items())[1:]]
        return rest

    @property
    def member_q(self, key):
        """
        member_q(key) returns a boolean as to whether the passed key is found in the ordered dictionary
        """
        if key in list(self.keys()):
            return True
        return False
