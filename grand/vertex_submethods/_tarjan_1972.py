class Tarjan1972:
    def __strong_connect__(self, vertex_set, tarjan_index, stack, strongly_connected_components):
        """
        Recursive routine detailed by Tarjan in http://epubs.siam.org/doi/10.1137/0201010 to find
        strongly connected components.

        Keyword arguments:
        vertex_set --
        tarjan_index --
        stack -- same as used in dfs
        strongly_connected_components -- list of lists (filled with vertices) defining strongly connected components
        """
        self['tarjan_index'] = tarjan_index
        self['tarjan_low_link'] = tarjan_index
        tarjan_index += 1
        stack.append(self)
        self['on_tarjan_stack'] = True

        for w in self["successors"]:
            if vertex_set[w]["tarjan_index"] is None:
                vertex_set[w].__strong_connect__(vertex_set, tarjan_index, stack, strongly_connected_components)
                self["tarjan_low_link"] = min(self["tarjan_low_link"], vertex_set[w]["tarjan_low_link"])

            elif vertex_set[w]["on_tarjan_stack"] is True:
                self["tarjan_low_link"] = min(self["tarjan_low_link"], vertex_set[w]["tarjan_index"])

        if self["tarjan_low_link"] == self["tarjan_index"]:
            strongly_connected_component = []

            flag = True
            while flag:
                w = stack.pop()
                w["on_tarjan_stack"] = False
                strongly_connected_component.append(w)

                if w == self:
                    flag = False

            strongly_connected_components.append(strongly_connected_component)
