import os
import math

class SVG():
        def __init__(self, vertex_radius, horizontal_padding, vertical_padding):
            self.radius = vertex_radius
            self.x_padding = horizontal_padding / 2
            self.y_padding = vertical_padding / 2

            self.file = ''

            self.newline = '\n'
            self.tab = '\t'

            self.close_bracket = '/>' + self.newline
            self.circle_close = '</circle>' + self.newline
            self.svg_close = '</svg>' + self.newline
            self.path_close = '</path>' + self.newline
            self.marker_close = '</marker>' + self.newline
            self.defs_open = '<defs>' + self.newline
            self.defs_close = '</defs>' + self.newline
            self.escaped_quote = '\''




        def start_group(self, g_id, g_class='GRAND_group'):
            g = '<g class="' + g_class + '" '
            if g_id is not None:
                g+= 'id="' + g_id + '" '
            g += '>'
            return g

        def end_group(self):
            return '</g>'


        def header(self):
            return '<?xml version="1.0" encoding="utf-8" ?>' + self.newline

        def circle(self, x=0, y=0, radius=1, stroke='black', stroke_width='1', fill='cyan', opacity='.9'):

            circle = '<circle cx="' + str(x + self.x_padding) + '" cy="' + str(y + self.y_padding) + '" r="' + str(radius) + \
                     '" stroke="' + str(stroke) + '" stroke-width="' + str(stroke_width) + '" fill="' + str(fill) + \
                     '" opacity="' + str(opacity) + '" ' + self.close_bracket

            return circle

        def rect(self, x=0, y=0, width=1, height=1, stroke='black', stroke_width='1', fill='cyan', opacity='.9', rotate=0):

            cx = x + self.x_padding - width / 2
            cy = y + self.y_padding - height / 2

            rect = '<rect x="' + str(cx) + '" y="' + str(cy) + '" width="' + str(width) + '" height="' + str(height) + \
                     '" stroke="' + str(stroke) + '" stroke-width="' + str(stroke_width) + '" fill="' + str(fill) + \
                     '" opacity="' + str(opacity) + '" '
            if str(rotate) != 0:
                # rect += 'transform="translate(' + str(cy / 2 ) + ', ' + str(cx / 2) + ') rotate(' + str(rotate) + ')" '
                rect += 'transform="rotate(' + str(rotate) + ' ' + str(cx + width / 2) + ' ' + str(cy + height / 2) + ')" '
                pass
            rect += self.close_bracket

            return rect


        def path(self, path_coordinates, fill='none', stroke='black', stroke_width='5', marker=True,
                 marker_position='end', marker_id='arrowhead', spacing_from_vertices=1.5, curved=True):
            # if line is not directly below, make it concave
            if curved:
                d_string = self.__curved_path_string__(path_coordinates, spacing_from_vertices)
            else:
                d_string = self.__straight_path_string__(path_coordinates, spacing_from_vertices)

            if curved:
                path = '<path d="' + d_string + '" fill="' + str(fill) + '" stroke="' + str(stroke) + \
                       '" stroke-width="' + str(stroke_width) + '" '
            else:
                path = '<polyline points="' + d_string + '" fill="' + str(fill) + '" stroke="' + str(stroke) + \
                       '" stroke-width="' + str(stroke_width) + '" '


            if marker:
                if marker_position == 'end':
                    path += 'marker-end="url(#' + str(marker_id) + ')" '
                if marker_position == 'middle':
                    path += 'marker-mid="url(#' + str(marker_id) + ')" '
                if marker_position == 'start':
                    path += 'marker-start="url(#' + str(marker_id) + ')" '

            path += self.close_bracket
            return path

        def __straight_path_string__(self, path_coordinates, spacing_from_vertices):
            path = self.__space_out_coordinate_path__(path_coordinates, spacing_from_vertices)
            d_string = ''
            for (x, y) in path:
                d_string += self.xy(x, y)
            return d_string

        def __curved_path_string__(self, path_coordinates, spacing_from_vertices):
            if len(path_coordinates) == 2:
                return self.__curved_path_string_len_2__(path_coordinates, spacing_from_vertices)
            elif len(path_coordinates) == 3:
                return self.__curved_path_string_len_3__(path_coordinates, spacing_from_vertices)
            else:
                return self.__curved_path_string_long__(path_coordinates, spacing_from_vertices)

        def __curved_path_string_len_2__(self, path, spacing_from_vertices):
            d_string = ''
            path = self.__space_out_coordinate_path__(path, spacing_from_vertices)
            ((x1, y1), (x2, y2)) = path
            d_string += 'M' + self.xy(x1, y1) + ' L ' + self.xy(x2, y2)
            return d_string

        def __curved_path_string_len_3__(self, path, spacing_from_vertices):
            path = self.__convert_path_coordinates_to_cubic_bezier_coordinates__(path)
            path = self.__space_out_coordinate_path__(path, spacing_from_vertices)
            d_string = ''
            for i, (x, y) in enumerate(path):
                if i == 0:
                    d_string += 'M ' + self.xy(x, y)
                elif i == 1:
                    d_string += 'C ' + self.xy(x, y)
                else:
                    d_string += self.xy(x, y)
            return d_string

        def __curved_path_string_long__(self, path, spacing_from_vertices):
            path = self.__convert_path_coordinates_to_cubic_bezier_coordinates__(path)
            path = self.__space_out_coordinate_path__(path, spacing_from_vertices)
            d_string = ''
            for i, (x, y) in enumerate(path):
                if i == 0:
                    d_string += 'M ' + self.xy(x, y)
                elif i == 1:
                    d_string += 'C ' + self.xy(x, y)
                elif i == 2 or i == 3:
                    d_string += self.xy(x, y)
                elif i % 2 == 0:
                    d_string += 'S ' + self.xy(x, y)
                else:
                    d_string += self.xy(x, y)
            return d_string

        def __convert_path_coordinates_to_cubic_bezier_coordinates__(self, path):
            bezier_coordinates = []
            bezier_coordinates.append(path[0])

            for i in range(len(path)):
                if i < len(path) - 2:
                    middle_1, middle_2, = self.__spread_middle_coordinate_by_radius__(path[i:i + 3])
                    bezier_coordinates.append(middle_1)
                    bezier_coordinates.append(middle_2)
            bezier_coordinates.append(path[-1])
            return bezier_coordinates

        def __spread_middle_coordinate_by_radius__(self, triple_coordinates):
            ((x1, y1), (x2, y2), (x3, y3)) = triple_coordinates

            if x2 == x1:
                x2_1 = x2
                y2_1 = y2 - self.radius
            else:
                slope_12 = (y2 - y1) / (x2 - x1)
                b_12 = y2 - slope_12 * x2

                y2_1 = y2 - self.radius
                x2_1 = (y2_1 - b_12) / slope_12

            if x3 == x2:
                x2_2 = x2
                y2_2 = y2 + self.radius
            else:
                slope_23 = (y3 - y2) / (x3 - x2)
                b_23 = y3 - slope_23 * x3

                y2_2 = y2 + self.radius
                x2_2 = (y2_2 - b_23) / slope_23

            return (x2_1, y2_1), (x2_2, y2_2)

        def __space_out_coordinate_path__(self, path, spacing_from_vertices):
            (x1, y1), (x2, y2) = path[0], path[1]

            # scale = 1.5

            if x2 == x1:
                x1_new = x1
                y1_new = y1 + self.radius * spacing_from_vertices
                path[0] = (x1_new, y1_new)

            else:
                slope = (y2 - y1) / (x2 - x1)
                b = y2 - slope * x2

                hypotenus = self.radius * spacing_from_vertices
                theta = math.atan(slope)
                y1_new = hypotenus * math.sin(theta)

                y1_new = hypotenus * math.sin(theta)
                x1_new = math.sqrt(hypotenus * hypotenus - y1_new * y1_new)

                if x1 < x2:
                    path[0] = (x1 + x1_new, y1 + y1_new)
                else:
                    path[0] = (x1 - x1_new, y1 - y1_new)


            (x1, y1), (x2, y2) = path[-2], path[-1]

            if x2 == x1:
                x2_new = x2
                y2_new = y2 - self.radius * spacing_from_vertices
                path[-1] = (x2_new, y2_new)

            else:
                slope = (y2 - y1) / (x2 - x1)
                b = y2 - slope * x2

                hypotenus = self.radius * spacing_from_vertices
                theta = math.atan(slope)
                y2_new = hypotenus * math.sin(theta)

                y2_new = hypotenus * math.sin(theta)
                x2_new = math.sqrt(hypotenus * hypotenus - y2_new * y2_new)

                if x1 < x2:
                    path[-1] = (x2 - x2_new, y2 - y2_new)
                else:
                    path[-1] = (x2 + x2_new, y2 + y2_new)

                # y2_new = y2 - self.radius # * 1.5
                # x2_new = (y2_new - b) / slope



            #
            # path[-1] = (x2_new, y2_new)
            return path

        def node_radius(self, radius):
            self.radius = radius

        def padding(self, x, y):
            self.x_padding = x
            self.y_padding = y

        def xy(self, x, y):
            return str(x + self.x_padding) + ' ' + str(y + self.y_padding) + ' '

        def background(self, height, width, fill):
            bg = '<rect width="'+str(width)+'" height="' + str(height) + '" fill="' + str(fill) + '" />' + self.newline
            return bg


        def marker(self, id='arrowhead', markerHeight='10', markerUnits='strokeWidth', markerWidth='20',
                   orient='auto', refX='0', refY='5', marker_path='M 0 10 l 3.128 -5 l -3.128 -5 l 20 5 l -20 5 Z',
                   marker_fill='black', marker_stroke='none', marker_opacity='1', viewbox='0 0 25 25'):
            defs = self.defs_open

            marker = self.tab+'<marker id="'+id+'" markerHeight="'+str(markerHeight)+'" markerUnits="'+str(markerUnits)+\
                     '" markerWidth="'+markerWidth+'" orient="'+str(orient)+'" refX="'+str(refX)+'" refY="'+str(refY) +\
                     '" viewBox="' + str(viewbox) + '">'+\
                     self.newline

            marker_path = self.tab+self.tab+'<path d="'+str(marker_path)+'" fill="'+marker_fill+'" stroke="'+marker_stroke+\
                          '" opacity="'+marker_opacity+'" />'

            end = self.tab+self.marker_close+self.newline+self.defs_close
            string = defs + marker + marker_path + end
            return string


        def number(self, x, y, radius, string):
            font_size = math.floor(radius / 2)
            text = '<text x="' + str(x + self.x_padding - font_size / 2) + '" y="' + str(y + self.y_padding + font_size / 2) + '" font-size="' + str(font_size) + '" stroke="black" fill="black" font-family="Verdana" >'
            text += self.newline + string + self.newline
            text += '</text>'
            return text
        def end(self):
            self.add(self.svg_close)

        def start(self, height, width):
            header = self.header()
            svg_open = '<svg \
            xmlns = "http://www.w3.org/2000/svg" \
            xmlns:ev = "http://www.w3.org/2001/xml-events" \
            xmlns:xlink = "http://www.w3.org/1999/xlink" \
            viewBox = "0 0 ' + str(width) + ' ' + str(height) + '" \
            preserveAspectRatio="xMidYMid meet" >' + self.newline
            # height =" ' + str(height) + '" width =" ' + str(width) + '" >' + self.newline

            self.add(header)
            self.add(svg_open)

        def add(self, string):
            self.file += string

        def write_out(self, filename='Magruder_et_al_2017_graph.svg', directory='Magruder_et_al_2017_graph_output', force=False):
            if not os.path.isdir(directory) and not force:
                print('Directory: ' + directory + ' does not exist! Force set to False. Call with Force=True to make the directory.')
                return
            if not os.path.isdir(directory) and force:
                os.mkdir(directory)

            fullname = os.path.join(directory, filename)

            if os.path.isfile(fullname) and not force:
                print('File: ' + filename + 'already exists! Force set to False. Call with Force=True to overwrite the file')

            else:
                file = open(fullname, 'w')
                file.write(self.file)
                file.close()
