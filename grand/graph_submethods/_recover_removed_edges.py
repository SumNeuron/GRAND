class RecoverRemovedEdges:
    def __get_pq_paths__(self):
        p_edges = self.edge_set.gather(self.edge_set.select(**{'class': lambda c: 'p_edge' in c})).values()
        q_edges = self.edge_set.gather(self.edge_set.select(**{'class': lambda c: 'q_edge' in c})).values()
        # if somehow edges are resorted, they will not corresopnd, but this ensures they correspond
        print(self.edge_set.gather(self.edge_set.select(**{'class': lambda c: 'p_edge' in c})))
        print(self.edge_set.gather(self.edge_set.select(**{'class': lambda c: 'q_edge' in c})))

        p_edges = sorted(p_edges, key=lambda e: e['target'])
        q_edges = sorted(q_edges, key=lambda e: e['source'])
        return [
            [
                p['source'],
                p['target'],
                q['source'],
                q['target']
            ] for (p, q) in zip(p_edges,q_edges)
        ]

    def __get_pq_paths__(self):
        p_edges = self.edge_set.gather(self.edge_set.select(**{'class': lambda c: 'p_edge' in c})).values()
        paths = []
        for pe in p_edges:
            s, t = pe.vw()
            p = [s, t]
            while self.vertex_set[t].dummy_q():
                s, t = self.edge_set[self.edge_set.select(**{'source': lambda s: s == t})[0]].vw()

            p.append(s)
            p.append(t)
            paths.append(p)
        return paths

    def __get_r_paths__(self):
        return [
        [e['source'], e['target']] for e in
            self.edge_set.gather(
                self.edge_set.select(**{'class': lambda c: 'regular' in c})
            ).values()
        ]
    # Reconstructing removed longed edges and getting coordinates of paths for smooth drawing
    def __get_p_and_q_edges_of_removed_edges__(self):
        p_edges = self.edge_set.select(**{'class': lambda c: c == 'p_edge'})
        q_edges = self.edge_set.select(**{'class': lambda c: c == 'q_edge'})

        matching_p_q_edges = []

        for p_edge in p_edges:
            p = int(p_edge.split('_')[-1])
            q_values = [int(q.split('_')[0]) for q in q_edges if int(q.split('_')[0]) >= p]

            differences = [q - p for q in q_values]
            q = q_values[q_values.index(min(differences) + p)]

            q_edge = [q_edge for q_edge in q_edges if int(q_edge.split('_')[0]) == q][0]

            matching_p_q_edges.append((p_edge, q_edge))

        return matching_p_q_edges

    def __get_all_edges_between_p_and_q_edges__(self, p_q_tuples):
        paths = []

        for (p, q) in p_q_tuples:

            if p.split('_')[-1] == q.split('_')[0]:
                paths.append([p, q])
                continue

            p_vertex = p.split('_')[-1]
            q_vertex = q.split('_')[0]

            next_edge = self.edge_set.select(**{'source': lambda s: s == p_vertex})[0]
            next_source, next_target = next_edge.split('_')[0], next_edge.split('_')[-1]

            path = [p]

            while next_target != q_vertex:
                path.append(next_edge)
                current_edge = next_edge
                current_source, current_target = next_source, next_target
                next_edge = self.edge_set.select(**{'source': lambda s: s == current_target})[0]
                next_source, next_target = next_edge.split('_')[0], next_edge.split('_')[-1]

            path.append(next_edge)
            path.append(q)

            paths.append(path)
        return paths

    def return_removed_edges(self):
        edge_keys = list(self.removed_edges.keys())
        for edge_key in edge_keys:
            edge_value = self.removed_edges.pop(edge_key, None)
            self.edge_set[edge_key] = edge_value
            s, t = edge_value.vw()
            # print(edge_key)
            self.vertex_set[s]['successors'].append(t)
            self.vertex_set[t]['predecessors'].append(s)

    def unreverse_all_edges(self):
        original_keys = list(self.edge_set.keys())
        while original_keys:
            edge_key = original_keys.pop()
            edge_value = self.edge_set[edge_key]
            if edge_value.reversed():
                nk = self.reverse_edge(edge_key)
                # print(edge_key, nk)

    def merge_pq_edge_with_removed_edges():
        pass
