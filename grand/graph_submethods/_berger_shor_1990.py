class BergerShor1990:
# https://dl.acm.org/citation.cfm?id=320203
    def berger_shor(self, graph, implementation=None):

        acyclic_edges = []
        cyclic_edges = []

        if implementation == "minimum_feedback_arc_set":
            strongly_connected_components = graph.vertex_set.__tarjan_strongly_connected_components__()
            edges_between = graph.edge_set.__edges_between_strongly_connected_components__(strongly_connected_components)

            for edge in edges_between:
                if self.edge_set[edge]['source'] == self.edge_set[edge]['target']:
                    continue
                acyclic_edges.append(edge)
                graph.remove_edge(edge)

        if len(graph.edge_set) is not 0:  ##if all edges removed because graph acylcic no need to run
            original_vertices = list(graph.vertex_set.items())
            while original_vertices:
                vertex_name, vertex_values = original_vertices.pop()
            # for vertex_name, vertex_values in graph.vertex_set.items():

                if vertex_values.out_degree() >= vertex_values.in_degree():
                    for target in vertex_values["successors"]:
                        if target == vertex_name:
                            continue
                        else:
                            acyclic_edges.append(vertex_name + "_" + target)
                else:
                    for source in vertex_values["predecessors"]:
                        acyclic_edges.append(source + "_" + vertex_name)
                graph.remove_vertex(vertex_name)

        for edge_key in self.edge_set:
            if edge_key not in acyclic_edges:
                cyclic_edges.append(edge_key)


        return cyclic_edges
