class DrawingAids:
    def __get_coordinates_of_path__(self, path):
        coordinates = []
        for i, edge in enumerate(path):
            if i == 0:
                local_coordinates = self.vertex_set[edge.split('_')[0]]['coordinates']
                coordinates.append((local_coordinates['x'], local_coordinates['y']))
                local_coordinates = self.vertex_set[edge.split('_')[-1]]['coordinates']
                coordinates.append((local_coordinates['x'], local_coordinates['y']))
                continue

            local_coordinates = self.vertex_set[edge.split('_')[-1]]['coordinates']
            coordinates.append((local_coordinates['x'], local_coordinates['y']))
        return coordinates

    def __get_coordinates_of_paths__(self, paths):
        all_coordinates = []
        for path in paths:
            all_coordinates.append(self.__get_coordinates_of_path__(path))
        return all_coordinates

    def __long_paths__(self):
        p_q_edges = self.__get_p_and_q_edges_of_removed_edges__()
        p_q_edge_paths = self.__get_all_edges_between_p_and_q_edges__(p_q_edges)
        return p_q_edge_paths

    def __lists_of_edge_coordinates__(self):
        real_edges = self.edge_set.select(**{'class': lambda c: 'regular' in c})
        real_edges = [[edge] for edge in real_edges]
        long_edges = self.__long_paths__()
        return self.__get_coordinates_of_paths__(real_edges + long_edges)

    # Method for drawing the graph
    def __width_of_canvas__(self):
        max_x = max(self.vertex_set.values(), key=lambda v: v['coordinates']['x'])['coordinates']['x']
        x_digits = len(str(int(max_x)))
        x_size = max([round(max_x, -x_digits), int(str(int(str(max_x)[0]) + 1) + '0' * (x_digits - 1))])
        return x_size * 1.2

    def __vertex_radius_from_canvas_width__(self, width, spacing=4):
        return width / max([len(layer) for layer in self.layering]) / 2 / spacing

    def __reset_y_positions__(self):
        for vertex in self.vertex_set.values():
            vertex['coordinates']['y'] = vertex['layer'] * self.minimum_vertex_separation

    def __adjust_y_positions__(self, min_separation=2.5):
        width = self.__width_of_canvas__()
        node_radius = self.__vertex_radius_from_canvas_width__(width)

        self.__reset_y_positions__()
        for vertex in self.vertex_set.values():
            vertex['coordinates']['y'] = vertex['layer'] * (self.minimum_vertex_separation + node_radius * min_separation)

    def __height_of_canvas__(self):
        max_y = max(self.vertex_set.values(), key=lambda v: v['coordinates']['y'])['coordinates']['y']
        y_digits = len(str(int(max_y)))
        y_size = max([round(max_y, -y_digits), int(str(int(str(max_y)[0]) + 1) + '0' * (y_digits - 1))])
        return y_size * 1.2

    def __image_padding__(self, width, height, radius):
        max_x = max(self.vertex_set.values(), key=lambda v: v['coordinates']['x'])['coordinates']['x']
        max_y = max(self.vertex_set.values(), key=lambda v: v['coordinates']['y'])['coordinates']['y']

        x_padding = width - max_x
        y_padding = height - max_y

        # while max_y + y_padding / 2 + radius > height:
        #     height *= 1.01
        # y_padding = height - max_y

        return x_padding, y_padding
