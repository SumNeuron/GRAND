from ..edge import Edge
class Helpers:
    def set_spans(self):
        """
        ASSUMES that the a layering has been made. Accordingly sets the span for all edges in the EdgeSet.
        :return: None
        """
        if self.layering is not []:
            try:
                self.edge_set.first['span']
            except KeyError:
                self.edge_set.__make_layering_keys__()
            self.edge_set.__set_spans__(self.layering)

    def remove_edge(self, edge_key):

        # In both: remove from edge_set
        if edge_key in self.edge_set and edge_key in self.removed_edges:
            edge = self.edge_set.pop(edge_key)
            u, w = edge.vw()

            # If the edges are not equivalent, update second edge.
            if edge != self.removed_edges[edge_key]:
                self.removed_edges[edge_key] = edge

            # Update successors and predecessors
            if w in self.vertex_set[u]['successors']:
                self.vertex_set[u]['successors'].remove(w)
                try:
                    self.vertex_set[u]['out_degree'] -= 1
                except KeyError:
                    pass

            if u in self.vertex_set[w]['predecessors']:
                self.vertex_set[w]['predecessors'].remove(u)
                try:
                    self.vertex_set[w]['in_degree'] -= 1
                except KeyError:
                    pass

        # In edge_key and not in removed edges
        elif edge_key in self.edge_set and edge_key not in self.removed_edges:
            # Remove from edge_set and add to removed edges
            edge = self.edge_set.pop(edge_key)
            u, w = edge.vw()


            self.removed_edges[edge_key] = edge

            # Update successors and predecessors
            if w in self.vertex_set[u]['successors']:
                self.vertex_set[u]['successors'].remove(w)
                try:
                    self.vertex_set[u]['out_degree'] -= 1
                except KeyError:
                    pass

            if u in self.vertex_set[w]['predecessors']:
                self.vertex_set[w]['predecessors'].remove(u)
                try:
                    self.vertex_set[w]['in_degree'] -= 1
                except KeyError:
                    pass

        # Not in edge_key but in removed edges
        elif edge_key not in self.edge_set and edge_key in self.removed_edges:
            # Edge already removed. Test for safety
            edge = self.removed_edges[edge_key]
            u, w = edge.vw()
            # Update successors and predecessors
            if w in self.vertex_set[u]['successors']:
                self.vertex_set[u]['successors'].remove(w)
                try:
                    self.vertex_set[u]['out_degree'] -= 1
                except KeyError:
                    pass
            if u in self.vertex_set[w]['predecessors']:
                self.vertex_set[w]['predecessors'].remove(u)
                try:
                    self.vertex_set[w]['in_degree'] -= 1
                except KeyError:
                    pass

        # Not in either
        else:
            pass
            # print("Edge: ", edge_key, " was not found in either the EdgeSet or the RemovedEdges.")

    def remove_vertex(self, vertex_key):
        vertex = self.vertex_set[vertex_key]
        edges = vertex.__edges__(vertex_key)
        for edge in edges:
            self.remove_edge(edge)

        for layer in self.layering:
            if vertex_key in layer:
                layer.remove(vertex_key)

        # if vertex.dummy_q():
        #     p = vertex['predecessors'][0]
        #     s = vertex['successors'][0]
        #     self.vertex_set[p]['successors'].append(s)
        #     self.vertex_set[s]['predecessors'].append(p)
        #     self.edge_set[p+"_"+s] = Edge(**{'source': p, 'target': t, "class":"dummy_edge"})

        del self.vertex_set[vertex_key]
        return vertex



    def reverse_edge(self, edge_key):
        """
        If one wants to reverse an edge, this function does so. Further it also updates the affected vertices so that
        the successors and predecessors are correct.
        :param edge_key: a string for querying the EdgeSet
        :return: None
        """

        source, target = self.edge_set[edge_key].vw()
        new_edge_key = self.edge_set.reverse(edge_key)
        self.vertex_set[source].__incident_edge_reversed__(str(target), "target")
        self.vertex_set[target].__incident_edge_reversed__(str(source), "source")
        return new_edge_key


    def edge_to_edge_key(self, edge):
        string = ''
        if type(edge) == Edge:
            u, w = edge.vw
            string = str(u) + "_" + str(w)
        elif type(edge) == list:
            u, w = edge[0], edge[1]
            string = str(u) + "_" + str(w)
        elif type(edge) == str:
            string = edge
        return string


    def __induce_layer__(self, layering):

        layering = [[str(v) for v in layer] for layer in layering]

        self.vertex_set.__update_subkeys_from_layering__(layering)
        self.layering = layering
        self.number_of_layers = len(layering)
        self.set_spans()
