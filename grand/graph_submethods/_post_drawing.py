from ..vertexset import VertexSet
from copy import deepcopy
class PostDrawing:
    def sugiyama_cleanup(self):
        self.return_removed_edges()
        # opq_paths = self.__get_pq_paths__()

        self.unreverse_all_edges()
        pq_paths = self.__get_pq_paths__()
        r_paths = self.__get_r_paths__()



        for i, edge in enumerate(r_paths):
            k = edge[0]+'_'+edge[-1]
            self.edge_set[k]['path'] = edge

        for i, edge in enumerate(pq_paths):
            k = edge[0]+'_'+edge[-1]
            self.edge_set[k]['path'] = edge

        dummies_to_keep = set([vertex for path in pq_paths for vertex in path])
        dummies = set(self.vertex_set.select(**{'class': lambda c: 'dummy' in c}))
        dummies_to_toss = dummies - dummies_to_keep
        vs = VertexSet()
        for dummy in dummies:
            if dummy in dummies_to_keep:
                vs[dummy] = deepcopy(self.vertex_set[dummy])
            self.remove_vertex(dummy)

        for k, v in vs.items():
            for p in pq_paths:
                if k in p:
                    if v['successors']:
                        v['predecessors'] = [p[1]]
                    else:
                        v['successors'] = [p[2]]
        return vs
