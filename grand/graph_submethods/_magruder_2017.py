from random import shuffle

class Snake:
    # Snake Method
    def __snake_crossings__(self, l_1=None, l_2=None, l_3=None, direction='down'):
        crossings = 0
        if direction == 'down':
            if l_3 == len(self.layering):
                crossings = self.__crossings_between_layers__(l_1, l_2)
            else:
                l_3 = self.layering[l_3]
                crossings = self.__crossings_between_layers__(l_1, l_2) + self.__crossings_between_layers__(l_2, l_3)

        else:
            if l_1 < 0:
                crossings = self.__crossings_between_layers__(l_2, l_3)
            else:
                l_1 = self.layering[l_1]
                crossings = self.__crossings_between_layers__(l_1, l_2) + self.__crossings_between_layers__(l_2, l_3)

        return crossings

    def __snake_up__(self, calling_vertex_key, predecessor_vertex_key, seen):
        calling_layer_index = self.vertex_set[calling_vertex_key]['layer']
        predecessor_layer_index = self.vertex_set[predecessor_vertex_key]['layer']


        calling_layer = self.layering[self.vertex_set[calling_vertex_key]['layer']]
        predecessor_layer = self.layering[self.vertex_set[predecessor_vertex_key]['layer']]

        calling_vertex_position = self.vertex_set[calling_vertex_key]['position_in_layer']
        predecessor_vertex_position = self.vertex_set[predecessor_vertex_key]['position_in_layer']

        moves_made = []
        counter = 1
        while counter:
            for vertex in predecessor_layer:
                if vertex == predecessor_vertex_key:
                    continue


                for successor in self.vertex_set[vertex]['successors']:
                    vertex_position = self.vertex_set[vertex]['position_in_layer']
                    successor_position = self.vertex_set[successor]['position_in_layer']

                    condition_1 = successor_position > calling_vertex_position and vertex_position < predecessor_vertex_position
                    condition_2 = successor_position < calling_vertex_position and vertex_position > predecessor_vertex_position
                    condition_3 = (predecessor_vertex_key, vertex, predecessor_vertex_position,
                                   vertex_position) not in moves_made

                    if (condition_1 or condition_2) and condition_3:
                        current_crossings = self.__snake_crossings__(predecessor_layer_index - 1,
                                                                     predecessor_layer,
                                                                     calling_layer, 'up')


                        self.__swap__(predecessor_vertex_key, vertex)
                        predecessor_vertex_position = self.vertex_set[predecessor_vertex_key]['position_in_layer']

                        new_crossings = self.__snake_crossings__(predecessor_layer_index - 1,
                                                                 predecessor_layer,
                                                                 calling_layer, 'up')

                        if new_crossings > current_crossings:
                            self.__swap__(predecessor_vertex_key, vertex)
                            predecessor_vertex_position = self.vertex_set[predecessor_vertex_key]['position_in_layer']


                        moves_made.append((predecessor_vertex_key, vertex, predecessor_vertex_position, vertex_position))
            counter -= 1

        for predecessor in self.vertex_set[predecessor_vertex_key]['predecessors']:
            if predecessor not in seen:
                seen.append(predecessor)
                self.__snake_up__(predecessor_vertex_key, predecessor, seen)

    def __snake_down__(self, calling_vertex_key, successor_vertex_key, seen):
        calling_layer_index = self.vertex_set[calling_vertex_key]['layer']
        successor_layer_index = self.vertex_set[successor_vertex_key]['layer']

        calling_layer = self.layering[self.vertex_set[calling_vertex_key]['layer']]
        successors_layer = self.layering[self.vertex_set[successor_vertex_key]['layer']]

        calling_vertex_position = self.vertex_set[calling_vertex_key]['position_in_layer']
        successor_vertex_position = self.vertex_set[successor_vertex_key]['position_in_layer']

        moves_made = []
        counter = 1

        while counter:
            for other_vertex in successors_layer:
                if other_vertex == successor_vertex_key:
                    continue

                for predecessor in self.vertex_set[other_vertex]['predecessors']:
                    vertex_position = self.vertex_set[other_vertex]['position_in_layer']
                    predecessor_position = self.vertex_set[predecessor]['position_in_layer']

                    condition_1 = predecessor_position > calling_vertex_position and vertex_position < successor_vertex_position
                    condition_2 = predecessor_position < calling_vertex_position and vertex_position > successor_vertex_position
                    condition_3 = (successor_vertex_key, other_vertex, successor_vertex_position, vertex_position) not in moves_made

                    if (condition_1 or condition_2) and condition_3:
                        current_crossings = self.__snake_crossings__(calling_layer,
                                                                     successors_layer,
                                                                     successor_layer_index + 1)

                        self.__swap__(successor_vertex_key, other_vertex)

                        successor_vertex_position = self.vertex_set[successor_vertex_key]['position_in_layer']

                        new_crossings = self.__snake_crossings__(calling_layer,
                                                                 successors_layer,
                                                                 successor_layer_index + 1)

                        if new_crossings > current_crossings:
                            self.__swap__(successor_vertex_key, other_vertex)
                            successor_vertex_position = self.vertex_set[successor_vertex_key]['position_in_layer']





                        moves_made.append((successor_vertex_key, other_vertex, successor_vertex_position, vertex_position))

            counter -= 1

        for successor in self.vertex_set[successor_vertex_key]['successors']:
            if successor not in seen:
                seen.append(successor)
                self.__snake_down__(successor_vertex_key, successor, seen)

    def snake_method(self):
        permutation_of_vertex_set = list(self.vertex_set.items())

        permutation_of_vertex_set = self.vertex_set.select(**{'predecessors': lambda l: len(l) == 0}) + self.vertex_set.select(**{'successors': lambda l: len(l) == 0})
        permutation_of_vertex_set = [(v, self.vertex_set[v]) for v in permutation_of_vertex_set]

        shuffle(permutation_of_vertex_set)

        down_seen = []
        up_seen = []

        counter = 1
        while counter:
            for vertex_key, vertex in permutation_of_vertex_set:
                for successor in vertex['successors']:
                    if successor not in down_seen:
                        down_seen.append(successor)
                        self.__snake_down__(vertex_key, successor, down_seen)
                for predecessor in vertex['predecessors']:
                    if predecessor not in up_seen:
                        up_seen.append(predecessor)
                        self.__snake_up__(vertex_key, predecessor, up_seen)

            counter -= 1
