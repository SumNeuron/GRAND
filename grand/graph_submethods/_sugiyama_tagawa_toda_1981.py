from copy import copy, deepcopy
from ..vertex import Vertex
class SugiyamaTagawaToda1981:
    # Methods for making dummy vertices and edges

    def generate_dummies(self, method='sugiyama_proper_layering'):
        _methods = ['efficient_sugiyama', 'sugiyama_proper_layering']
        self.edge_set.__make_dummy_keys__()
        self.vertex_set.__make_dummy_keys__()

        if method == 'efficient_sugiyama':
            self.efficient_sugiyama()
        elif method == 'sugiyama_proper_layering':
            self.sugiyama_proper_layering()

    def sugiyama_proper_layering(self):
        # Make a copy of the edge set to play with and remove edges from without worry.
        # Further prevents going through new edges. No need to worry about those.
        original_edge_set = deepcopy(self.edge_set)
        original_edge_set_keys = list(original_edge_set.keys())

        # Go through the edges in the original edge_set
        for edge in original_edge_set_keys:
            if original_edge_set[edge].needs_dummy_q():
                u, w = self.edge_set[edge].vw()



                span = self.edge_set[edge]['span']
                old_edge = self.edge_set[edge]


                source_layer, target_layer = self.vertex_set[u]['layer'], self.vertex_set[w]['layer']
                self.remove_edge(edge)

                # while the span is greater than one we need to collectively add edges
                # until the graph is properly layered. When the the span is one then we can use the original target
                current_source = u
                layer_index = 0
                while span is not 1:




                    # Bookkeeping
                    span -= 1
                    layer_index += 1

                    # Ensure not overwriting an existing vertex key
                    new_vertex = str(len(self.vertex_set) + 1)
                    while new_vertex in self.vertex_set:
                        new_vertex = str(int(new_vertex) + 1)

                    # Add edge
                    new_edge = current_source + "_" + new_vertex
                    if self.vertex_set[current_source]['class'] == 'regular':
                        new_edge_object = deepcopy(old_edge)
                        new_edge_object['source'] = current_source
                        new_edge_object['target'] = new_vertex
                        new_edge_object['class'] = 'p_edge'
                        self.edge_set[new_edge] = new_edge_object
                    else:
                        new_edge_object = deepcopy(old_edge)
                        new_edge_object['source'] = current_source
                        new_edge_object['target'] = new_vertex
                        new_edge_object['class'] = 'inner_segment_dummy_edge'
                        self.edge_set[new_edge] = new_edge_object

                    # Update previous vertex successors and make new vertex
                    self.vertex_set[current_source]['successors'].append(new_vertex)
                    self.vertex_set[new_vertex] = Vertex(**{'name': new_vertex, 'predecessors': [current_source],
                                                            'layer': source_layer + layer_index,
                                                            'position_in_layer': len(self.layering[source_layer + layer_index]),
                                                            'class': 'dummy_vertex'})

                    # Add new vertex to layer.
                    self.layering[source_layer + layer_index].append(new_vertex)
                    current_source = new_vertex

                # Span is one. Use original target.
                self.vertex_set[current_source]["successors"].append(w)

                new_edge = current_source + "_" + w
                new_edge_object = deepcopy(old_edge)
                new_edge_object['source'] = current_source
                new_edge_object['target'] = w
                new_edge_object['class'] = 'q_edge'

                self.edge_set[new_edge] = new_edge_object
                self.vertex_set[w]['predecessors'].append(current_source)
                # self.layering[target_layer - 1].append(current_source)


    # def __donw_up__(self):
    #     i = 0
    #
    #
    # def __k__(self, layer_i, layer_j, ordering_i, ordering_j):
    #     edges = self.edge_set.select(**{
    #         'source': lambda s: s in ordering_i+ordering_j,
    #         'target': lambda t: t in ordering_i+ordering_j
    #     })
    #
    #     for v_i in ordering_i:
    #         for v_j in o
