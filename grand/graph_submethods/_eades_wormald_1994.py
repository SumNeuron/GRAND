 # https://link.springer.com/article/10.1007/BF01187020
from statistics import median_high, median_low
from ..ond import OrderedNestedDictionary
class EadesWormald1994:
# Median based crossing reduction heuristic
    def sugiyama_eades_wormald_modified_heuristic(self):
        counter = 1
        while counter:
            self.__sugiyama_eades_wormald_down__()
            self.__sugiyama_eades_wormald_up__()
            counter -= 1

    def __sugiyama_eades_wormald_down__(self):
        counter = 1
        while counter:
            for layer_index in range(len(self.layering) - 1):
                self.__sugiyama_eades_wormald_sort_of_next_layer__(layer_index)
            counter -= 1

    def __sugiyama_eades_wormald_up__(self):
        counter = 1
        while counter:
            for layer_index in range(len(self.layering) - 1, 0, -1):
                self.__sugiyama_eades_wormald_sort_of_previous_layer__(layer_index)
            counter -= 1

    def __sugiyama_eades_wormald_sort_of_next_layer__(self, layer_index):
        fixed_layer = self.layering[layer_index]
        free_layer = self.layering[layer_index + 1]

        current_crossings = self.__crossings_between_layers__(fixed_layer, free_layer)

        vertex_median_tuples = [self.__vertex_median__(vertex) for vertex in free_layer]
        vertex_median_tuples = sorted(vertex_median_tuples, key=lambda t: self.vertex_set[t[0]].in_degree())
        vertices_sorted_by_median_neighbor_position = sorted(vertex_median_tuples, key=lambda t: t[1])

        new_layer = [v for (v, m) in vertices_sorted_by_median_neighbor_position]

        new_crossings = self.__crossings_between_layers__(fixed_layer, new_layer)

        if new_crossings < current_crossings:
            self.__commit_new_layer__(layer_index + 1, new_layer)

    def __sugiyama_eades_wormald_sort_of_previous_layer__(self, layer_index):
        fixed_layer = self.layering[layer_index]
        free_layer = self.layering[layer_index - 1]

        current_crossings = self.__crossings_between_layers__(free_layer, fixed_layer)


        vertex_median_tuples = [self.__vertex_median__(vertex, direction='up') for vertex in free_layer]
        vertex_median_tuples = sorted(vertex_median_tuples, key=lambda t: self.vertex_set[t[0]].in_degree())
        vertices_sorted_by_median_neighbor_position = sorted(vertex_median_tuples, key=lambda t: t[1])

        new_layer = [v for (v, m) in vertices_sorted_by_median_neighbor_position]

        new_crossings = self.__crossings_between_layers__(new_layer, fixed_layer)

        if new_crossings < current_crossings:
            self.__commit_new_layer__(layer_index - 1, new_layer)

    # Update vertices in a layer
    def __commit_new_layer__(self, index, layer):
        self.layering[index] = layer
        for vertex in layer:
            self.vertex_set[vertex]['position_in_layer'] = layer.index(vertex)


    # Median position of vertex
    def __vertex_median__(self, vertex_key, direction='down', median_type='low', result='tuple'):
        median_pos = None
        if direction == 'down':
            median_pos = self.__vertex_median_down__(vertex_key, median_type)

        if direction == 'up':
            median_pos = self.__vertex_median_up__(vertex_key, median_type)

        if result == 'tuple':
            return (vertex_key, median_pos)
        else:
            return median_pos

    def __vertex_median_down__(self, vertex_key, median_type='low'):
        pos = 'position_in_layer'
        vertex_layer_index = self.vertex_set[vertex_key]['layer']
        median_pos = None

        fixed_layer = self.layering[vertex_layer_index - 1]
        free_layer = self.layering[vertex_layer_index]

        vertex_edges_to_fixed_layer = self.edge_set.select(**{'source': lambda s: s in fixed_layer,
                                                              'target': lambda t: t == vertex_key})
        neighbors_in_fixed_layer = [self.edge_set[edge]['source'] for edge in vertex_edges_to_fixed_layer]
        neighbors_positions = [self.vertex_set[neighbor][pos] for neighbor in neighbors_in_fixed_layer]

        if neighbors_positions:
            if median_type == 'low':
                median_pos = median_low(neighbors_positions)
            if median_type == 'high':
                median_pos = median_high(neighbors_positions)
        else:
            vertex_successors = self.vertex_set[vertex_key]['successors']
            if vertex_successors:
                if median_type == 'low':
                    median_pos = self.vertex_set.gather(vertex_successors).median_low_of(pos, 'subvalue')
                if median_type == 'high':
                    median_pos = self.vertex_set.gather(vertex_successors).median_high_of(pos, 'subvalue')
            else:
                median_pos = self.vertex_set[vertex_key][pos]
        return median_pos

    def __vertex_median_up__(self, vertex_key, median_type='low'):
        pos = 'position_in_layer'
        vertex_layer_index = self.vertex_set[vertex_key]['layer']
        median_pos = None

        free_layer = self.layering[vertex_layer_index]
        fixed_layer = self.layering[vertex_layer_index + 1]

        vertex_edges_to_fixed_layer = self.edge_set.select(**{'source': lambda s: s == vertex_key,
                                                              'target': lambda t: t in fixed_layer})
        neighbors_in_fixed_layer = [self.edge_set[edge]['target'] for edge in vertex_edges_to_fixed_layer]
        neighbors_positions = [self.vertex_set[neighbor][pos] for neighbor in neighbors_in_fixed_layer]

        if neighbors_positions:
            if median_type == 'low':
                median_pos = median_low(neighbors_positions)
            if median_type == 'high':
                median_pos = median_high(neighbors_positions)
        else:
            vertex_predecessors = self.vertex_set[vertex_key]['predecessors']
            if vertex_predecessors:
                if median_type == 'low':
                    median_pos = self.vertex_set.gather(vertex_predecessors).median_low_of(pos, 'subvalue')
                if median_type == 'high':
                    median_pos = self.vertex_set.gather(vertex_predecessors).median_high_of(pos, 'subvalue')
            else:
                median_pos = self.vertex_set[vertex_key][pos]

        return median_pos
