from ..vertex import Vertex
from ..vertexset import VertexSet
from ..edge import Edge
from ..edgeset import EdgeSet
class ReconFromPaths:
    def path_to_edges_keys(self, path):
        return ["_".join(path[i:i+2]) for i in range(len(path)-1)]

    def path_to_edges_vertices(self, path):
        return [path[i:i+2] for i in range(len(path)-1)]

    def paths_to_vertices(self, paths):
        return list(set([vertex for path in paths for vertex in path]))

    def paths_to_edges_vertices(self, paths):
        return [edge for path in paths for edge in self.path_to_edges_vertices(path)]

    def paths_to_edges_keys(self, paths):
        return [edge for path in paths for edge in self.path_to_edges_keys(path)]

    def reconstruct_from_paths(self, paths):
        vertices_in_paths = self.paths_to_vertices(paths)
        edge_vertices_in_paths = self.paths_to_edges_vertices(paths)

        vertex_set = VertexSet()
        for vertex in vertices_in_paths:
            vertex_set[vertex] = Vertex()

        edge_set = EdgeSet()
        for source, target in edge_vertices_in_paths:
            edge_set[source+'_'+target]= Edge(**{'source': source, 'target': target})
            vertex_set[source]['successors'].append(target)
            vertex_set[target]['predecessors'].append(source)
        graph = self.__class__()
        graph.vertex_set = vertex_set
        graph.edge_set = edge_set
        return graph
