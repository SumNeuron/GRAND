class CalculateEdgeCrossings:
    # Methods for tabulating edge crossings
    def __calculate_crossings__(self, action='return'):
        number_of_crossings = 0
        crossings = []
        for layer_index, layer in enumerate(self.layering):
            for index, v in enumerate(layer[:-1]):
                for u in layer[index + 1:]:
                    for y in self.vertex_set[v]['successors']:
                        for w in self.vertex_set[u]['successors']:
                            y_pos = self.vertex_set[y]['position_in_layer']
                            w_pos = self.vertex_set[w]['position_in_layer']

                            if y_pos > w_pos:
                                number_of_crossings += 1
                                crossings += [v + '_' + y + '---' + u + '_' + w]

        if action == 'return':
            return number_of_crossings
        elif action == 'set':
            self.number_of_crossings = number_of_crossings
        elif action == 'show_crossings':
            for i, crossing in enumerate(crossings):
                print(i + 1, '\t|\t', crossing)

    def __calculate_crossings_in_layer__(self, layer_index=0, action='return'):
        number_of_crossings = 0
        crossings = []
        layer = self.layering[layer_index]
        for index, v in enumerate(layer[:-1]):
            for u in layer[index + 1:]:
                for y in self.vertex_set[v]['successors']:
                    for w in self.vertex_set[u]['successors']:
                        y_pos = self.vertex_set[y]['position_in_layer']
                        w_pos = self.vertex_set[w]['position_in_layer']
                        if y_pos > w_pos:
                            number_of_crossings += 1
                            crossings += [v + '_' + y + '---' + u + '_' + w]

        if action == 'return':
            return number_of_crossings
        elif action == 'set':
            self.number_of_crossings = number_of_crossings
        elif action == 'show_crossings':
            for i, crossing in enumerate(crossings):
                print(i, '\t|\t', crossing)

    # Lets one look at crossings in either downward or upward direction
    def __crossings_between_layers__(self, top_layer, bottom_layer):
        # v --> y
        # u --> w
        pos = 'position_in_layer'
        crossings = 0
        for i, v in enumerate(top_layer):
            for u in top_layer[i:]:
                for y in [z for z in bottom_layer if z in self.vertex_set[v]['successors']]:
                    for w in [z for z in bottom_layer if z in self.vertex_set[u]['successors']]:
                        pos_v = top_layer.index(v)
                        pos_u = top_layer.index(u)
                        pos_y = bottom_layer.index(y)
                        pos_w = bottom_layer.index(w)

                        c_1 = pos_v < pos_u and pos_w < pos_y
                        c_2 = pos_v > pos_u and pos_w > pos_y

                        if c_1 or c_2:
                            crossings += 1
        return crossings
