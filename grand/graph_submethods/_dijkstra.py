import heapq
from math import inf
from collections import deque
class Dijkstra:
    def k_dijkstra(self, start, stop, num_paths):
        P = set() # set of shortest paths from start to lists_of_paths
        count_u = { v: 0 for v in self.vertex_set} # number of shortest paths found to node u
        B = [] # heap containing paths
        heapq.heappush(B, [start])
        while B:
            P_u = heapq.heappop(B) # shortest cost path in B with cost C
            B


    def dijkstra_dist(self, a, b):
        d = self.vertex_set[a]['dijkstra_distance'] + 1
        return d

    def dijkstra(self, start, stop=None, dist_func=dijkstra_dist, flow="with"):
        flow = 'successors' if flow == 'with' else 'predecessors'

        self.vertex_set.add_subkey('dijkstra_distance', inf)
        self.vertex_set.add_subkey('dijkstra_previous', None)

        # dist = {v: inf for v in self.vertex_set}
        # prev = {v: None for v in self.vertex_set}
        q = deque()
        [q.append(v) for v in self.vertex_set]

        self.vertex_set[start]['dijkstra_distance'] = 0
        # dist[start] = 0

        while q:
            # u = min(q, key=lambda v: dist[v])
            u = min(q, key=lambda v: self.vertex_set[v]['dijkstra_distance'])
            q.remove(u)

            if stop is not None and u == stop:
                return self.dijkstra_reconstruct(stop)

            for neighbor in self.vertex_set[u][flow]:
                alt = self.vertex_set[u]['dijkstra_distance'] + dist_func(self, u, neighbor)
                # alt = dist[u] + dist_func(self, u, neighbor)
                # if alt < dist[neighbor]:
                if alt < self.vertex_set[neighbor]['dijkstra_distance']:
                    self.vertex_set[neighbor]['dijkstra_distance'] = alt
                    self.vertex_set[neighbor]['dijkstra_previous'] = u
                    # dist[neighbor] = alt
                    # prev[neighbor] = u

        dist = {v: self.vertex_set[v]['dijkstra_distance'] for v in self.vertex_set}
        prev = {v: self.vertex_set[v]['dijkstra_previous'] for v in self.vertex_set}
        self.vertex_set.delete_subkey('dijkstra_distance')
        self.vertex_set.delete_subkey('dijkstra_previous')

        return prev


    def dijkstra_reconstruct(self, target, prev=None):
        s = []
        u = target

        if prev:
            while prev[u]:
                s.append(u)
                u = prev[u]
            s.append(u)
            s.reverse()
            return s
        
        while self.vertex_set[u]['dijkstra_previous']:
            s.append(u)
            u = self.vertex_set[u]['dijkstra_previous']
        s.append(u)
        s.reverse()
        return s
