from copy import deepcopy
from time import clock
class DepthFirstSearch:
    def depth_first_search(self, start, stop, max_depth, number_of_paths_to_find, direction='successors', timeout=float('Inf')):
        if direction not in ['successors', 'predecessors']:
            return []
        # self.vertex_set.add_subkey('depth_first_search_recursion', False)
        paths_found = []
        start_time = clock()
        self.__depth_first_search_recursion__(start, stop, 1, max_depth, number_of_paths_to_find, [], paths_found, direction, start_time, timeout)

        return paths_found

        # self.vertex_set.delete_subkey('depth_first_search_recursion')

    def __depth_first_search_recursion__(self, current, goal,
                                         current_depth, max_depth,
                                         number_of_paths_to_find, current_path, paths_found,
                                         direction, start_time, timeout):

        if clock() - start_time > timeout:
            return
        # print(current_path, current, goal)
        current_path.append(current)
        # self.vertex_set[current]['depth_first_search_recursion'] = True
        if current_depth > max_depth:
            current_path.pop()
            return

        if current == goal:
            # if still looking for paths add more paths
            if len(paths_found) < number_of_paths_to_find:
                paths_found.append(deepcopy(current_path))
            # remove current vertex and keep seraching for alternatives
            current_path.pop()
            return

        # hack to ensure we dont dig too deep if the goal is in current layer
        elif goal in self.vertex_set[current][direction]:
            current_path.append(goal)
            if len(paths_found) < number_of_paths_to_find:
                paths_found.append(deepcopy(current_path))

            current_path.pop()
            return

        else:
            # current isnt goal and goal isnt in current's horizon
            for neighbor in self.vertex_set[current][direction]:
                # if not self.vertex_set[successor]['depth_first_search_recursion']:
                new_depth = current_depth if self.vertex_set[neighbor].dummy_q() else current_depth + 1
                self.__depth_first_search_recursion__(
                    neighbor, goal,
                    new_depth, max_depth,
                    number_of_paths_to_find, current_path, paths_found,
                    direction, start_time, timeout
                )



        current_path.pop()
        # self.vertex_set[current]['depth_first_search_recursion'] = False
