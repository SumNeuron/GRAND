import math
from copy import copy, deepcopy
from itertools import chain, groupby
from math import floor, ceil
from operator import itemgetter
from random import shuffle
from statistics import median_high, median_low

class BrandesKopf2002:
    # Method for horizontal coordinates assignment (Brandes Koepf)
    def horizontal_coordinate_assignment(self, minimum_vertex_spacing, directions=["upper_left", "upper_right", "lower_left", "lower_right"]):
        delta = minimum_vertex_spacing
        self.vertex_set.__make_brandes_kopf_keys__()
        self.edge_set.__make_crossing_type_keys__()

        self.__mark_type_1_conflicts__()

        for direction in directions:
            self.__vertical_alignment__(direction=direction)
            self.__horizontal_compaction__(delta, direction=direction)

        self.__balance_coordinate_assignments__(minimum_vertex_spacing, directions=directions)

        # print(self.vertex_set)

        self.vertex_set.__remove_brandes_kopf_keys__()
        self.edge_set.__remove_crossing_types_keys__()


    def __upper_neighbors__(self, vertex_key, layering=None, v_type='dummy'):
        # print('upper_neighbors', vertex_key)
        v_dict = self.vertex_set[vertex_key]
        # if v_type == 'dummy' and v_type not in v_dict['class']:
        #     return []


        if layering is None:
            layering = self.layering

        pred = v_dict['predecessors']
        succ = v_dict['successors']

        v_layer = v_dict['layer']
        if v_layer - 1 < 0:
            return []

        neighbors_in_layer = [v for v in pred+succ if v in layering[v_layer-1]]
        return neighbors_in_layer

    def __lower_neighbors__(self, vertex_key, layering=None, v_type='dummy'):
        # print('upper_neighbors', vertex_key)
        v_dict = self.vertex_set[vertex_key]
        # if v_type == 'dummy' and v_type not in v_dict['class']:
        #     return []

        if layering is None:
            layering = self.layering

        pred = v_dict['predecessors']
        succ = v_dict['successors']

        v_layer = v_dict['layer']
        if v_layer + 1 >= len(layering):
            return []

        neighbors_in_layer = [v for v in pred+succ if v in layering[v_layer+1]]
        return neighbors_in_layer


    def __mark_type_1_conflicts__(self):
        # Type 0: non-inner segments cross. Resolved in greedly left fashion
        # Type 1: non-inner segment crosses an inner segment, resolve in favor of inner segment, marked by algorithm 1
        # Type 2: inner segments cross, mark as non-vertical and ignored for alignment

        # inner-segments will not appear on the first (0) or last layer (h); further this algorithm looks at layer i and
        #  i + 1. Thus no need to look at layer h-1, as it is resolved when calling h-2, where h is the number of layers

        # i = layer_index
        # h = number_of_layers
        # v_l_1_ip1 = v^(i+1)_(l_1) i.e. vertex in layer (i+1) at position (l_1)


        align = 'align'
        brandes_kopf = 'brandes_kopf'
        coordinates = 'coordinates'
        crossing_types = 'crossing_types'
        layer = 'layer'
        position_in_layer = 'position_in_layer'
        root = 'root'
        shift = 'shift'
        sink = 'sink'

        a = align
        bk = brandes_kopf
        ct = crossing_types
        l = layer
        pos = position_in_layer
        r = root
        sf = shift
        sk = sink
        x = 'x'
        xy = coordinates

        number_of_layers = len(self.layering)
        for i in range(1, number_of_layers - 1):
            k_0 = -1
            l = 0  # index
            # maintains the upper neighbors v_k0_i and v_k1_i of the two closest inner segments
            # upper neighbors v_{k_0}^i and v_{k_1}^1
            for l_1 in range(len(self.layering[i+1])):
                # for each vertex in the layer below

                v_l1_ip1 = self.layering[i + 1][l_1] # vertex l1 in layer i+1

                condition_1 = l_1 == len(self.layering[i + 1]) - 1 #if last vertex in layer

                # incident_edges = self.vertex_set.__inner_segments_of__(vertex_key=v_l1_ip1, which='predecessors')
                incident_edges = self.edge_set.select(**{'target': lambda t: t == v_l1_ip1 and self.vertex_set[t].dummy_q(),
                                                         'source': lambda s: self.vertex_set[s][l] == self.vertex_set[v_l1_ip1][l] - 1 and not self.vertex_set[s].dummy_q()
                                                         })

                if condition_1 or incident_edges:
                    k_1 = len(self.layering[i]) - 1

                    if incident_edges:
                        predecessors = self.vertex_set[v_l1_ip1]['predecessors'] # upper neighbors
                        k_1 = self.vertex_set[predecessors[0]][pos]

                    while l <= l_1:
                        # v_l_ip1 = self.layering[i + 1][l]
                        # other_predecessors = self.vertex_set.select(**{'successors': lambda s: v_l_ip1 in s,
                        #                                                'layer': lambda lay: lay == i})
                        # for predecessor in other_predecessors:
                        for predecessor in self.vertex_set[self.layering[i + 1][l]]['predecessors']:

                            k = self.layering[i].index(predecessor)

                            if predecessor not in self.layering[i]:
                                continue
                            if k < k_0 or k > k_1:
                                self.edge_set[predecessor + "_" + self.layering[i + 1][l]][ct]['type_1'] = True

                        l += 1
                    k_0 = k_1

    def __mark_type_1_conflicts__(self):
        # Type 0: non-inner segments cross. Resolved in greedly left fashion
        # Type 1: non-inner segment crosses an inner segment, resolve in favor of inner segment, marked by algorithm 1
        # Type 2: inner segments cross, mark as non-vertical and ignored for alignment

        # inner-segments will not appear on the first (0) or last layer (h); further this algorithm looks at layer i and
        #  i + 1. Thus no need to look at layer h-1, as it is resolved when calling h-2, where h is the number of layers

        # i = layer_index
        # h = number_of_layers
        # v_l_1_ip1 = v^(i+1)_(l_1) i.e. vertex in layer (i+1) at position (l_1)

        a = 'align'
        bk = 'brandes_kopf'
        xy = 'coordinates'
        ct = 'crossing_types'
        l = 'layer'
        pos = 'position_in_layer'
        r = 'root'
        sf = 'shift'
        sk = 'sink'
        x = 'x'

        number_of_layers = len(self.layering)
        for i in range(1, number_of_layers - 1):
            k_0 = -1
            l = 0  # index
            for l_1 in range(len(self.layering[i+1])):
                v_l1_ip1 = self.layering[i + 1][l_1] # vertex l1 in layer i+1

                v_l1_ip1_pred = self.vertex_set[v_l1_ip1]['predecessors']
                v_l1_ip1_succ = self.vertex_set[v_l1_ip1]['successors']

                condition_1 = l_1 == len(self.layering[i + 1]) - 1 #if last vertex in layer
                upper_neighbors = self.__upper_neighbors__(v_l1_ip1)

                if condition_1 or upper_neighbors:
                    k_1 = len(self.layering[i]) - 1

                    if upper_neighbors:
                        k_1 = self.vertex_set[upper_neighbors[0]][pos]

                    while l <= l_1:
                        v_l_ip1 = self.layering[i + 1][l]
                        upper_neighbors_v_l_ip1 = self.__upper_neighbors__(v_l_ip1)
                        for up_n in upper_neighbors_v_l_ip1:
                            k = self.layering[i].index(up_n)

                            if k < k_0 or k > k_1:
                                e1 = up_n+"_"+v_l_ip1
                                e2 = v_l_ip1+"_"+up_n
                                for e in [e1, e2]:
                                    if e in self.edge_set:
                                        self.edge_set[e][ct]['type_1'] = True



                        l += 1
                    k_0 = k_1

    def __eliminate_type_2_conflicts__(self):
        type_2_edges = self.edge_set.select(**{'source': lambda s: 'dummy' in self.vertex_set[s]['class'],
                                               'target': lambda t: 'dummy' in self.vertex_set[t]['class']})

        for i, e1 in enumerate(type_2_edges):
            for e2 in type_2_edges[i:]:
                if self.vertex_set[self.edge_set[e1]['source']]['layer'] != \
                        self.vertex_set[self.edge_set[e2]['source']]['layer']:
                    continue

                if self.vertex_set[self.edge_set[e1]['target']]['layer'] != \
                        self.vertex_set[self.edge_set[e2]['target']]['layer']:
                    continue

                p11 = self.vertex_set[self.edge_set[e1]['source']]['position_in_layer']
                p12 = self.vertex_set[self.edge_set[e1]['target']]['position_in_layer']
                p21 = self.vertex_set[self.edge_set[e2]['source']]['position_in_layer']
                p22 = self.vertex_set[self.edge_set[e2]['target']]['position_in_layer']

                if (p11 < p21 and p22 < p12) or (p11 > p21 and p22 > p12):
                    print('Type 2 crossing found between', e1, e2)
                    self.vertex_set[self.edge_set[e1]['target']]['position_in_layer'] = p22
                    self.vertex_set[self.edge_set[e2]['target']]['position_in_layer'] = p12
                    self.__eliminate_type_2_conflicts__()

    def __vertical_alignment_upper_left__(self, a, bk, ct, l, g, pos, r, root, sf, sk, x, xy):
        for layer_index, layer in enumerate(self.layering):
            # r is 1 less than the starting index. In Brandes and Koepf they use 1 indexed arrays. Python is 0
            # indexed.
            r = -1

            for vertex_index, vertex in enumerate(layer):
                v_k = vertex

                # want median of ALL upper neighbors, not just those in the preceding layer
                upper_neighbors = self.vertex_set[v_k]['predecessors']
                # sort to ensure that they are in order as they appear in the layer
                upper_neighbors = sorted(upper_neighbors, key=lambda pred: self.vertex_set[pred][pos])

                if upper_neighbors:
                    # d is number / index of upper neighbors, but our index is zero based.
                    d = len(upper_neighbors)
                    m_value = (d + 1) / 2

                    for m in [floor(m_value) - 1, ceil(m_value) - 1]:

                        if self.vertex_set[v_k][bk][g][a] == v_k:
                            u_m = upper_neighbors[m]

                            u_m_v_k = u_m + '_' + v_k

                            condition_1 = self.edge_set[u_m_v_k][ct]['type_1']
                            condition_2 = r < self.vertex_set[u_m][pos]

                            if not condition_1 and condition_2:
                                self.vertex_set[u_m][bk][g][a] = v_k
                                self.vertex_set[v_k][bk][g][root] = self.vertex_set[u_m][bk][g][root]
                                self.vertex_set[v_k][bk][g][a] = self.vertex_set[v_k][bk][g][root]
                                r = self.vertex_set[u_m][pos]

    def __vertical_alignment_upper_left__(self, a, bk, ct, l, g, pos, r, root, sf, sk, x, xy):
        for layer_index, layer in enumerate(self.layering):
            # r is 1 less than the starting index. In Brandes and Koepf they use 1 indexed arrays. Python is 0
            # indexed.
            r = -1
            for vertex_index, vertex in enumerate(layer):
                v_k = vertex

                # want median of ALL upper neighbors, not just those in the preceding layer
                upper_neighbors = self.__upper_neighbors__(v_k)
                # sort to ensure that they are in order as they appear in the layer
                upper_neighbors = sorted(upper_neighbors, key=lambda pred: self.vertex_set[pred][pos])

                if upper_neighbors:
                    # d is number / index of upper neighbors, but our index is zero based.
                    d = len(upper_neighbors)
                    m_value = (d + 1) / 2

                    for m in [floor(m_value) - 1, ceil(m_value) - 1]:

                        if self.vertex_set[v_k][bk][g][a] == v_k:
                            u_m = upper_neighbors[m]

                            u_m_v_k = u_m + '_' + v_k
                            v_k_u_m = v_k + '_' + u_m

                            condition_1 = False
                            for e in [u_m_v_k, v_k_u_m]:
                                if e in self.edge_set:
                                    condition_1 = self.edge_set[e][ct]['type_1']
                                    if condition_1:
                                        break

                            condition_2 = r < self.vertex_set[u_m][pos]

                            if not condition_1 and condition_2:
                                self.vertex_set[u_m][bk][g][a] = v_k
                                self.vertex_set[v_k][bk][g][root] = self.vertex_set[u_m][bk][g][root]
                                self.vertex_set[v_k][bk][g][a] = self.vertex_set[v_k][bk][g][root]
                                r = self.vertex_set[u_m][pos]

    def __vertical_alignment_upper_right__(self, a, bk, ct, l, g, pos, r, root, sf, sk, x, xy):
        for layer_index, layer in enumerate(self.layering):
            r = len(max(self.layering, key=lambda l: len(l)))

            for vertex in reversed(layer):
                v_k = vertex

                # want median of ALL upper neighbors, not just those in the preceding layer
                upper_neighbors = self.vertex_set[v_k]['predecessors']
                # sort to ensure that they are in order as they appear in the layer
                upper_neighbors = sorted(upper_neighbors, key=lambda pred: self.vertex_set[pred][pos])



                if upper_neighbors:
                    # d is number / index of upper neighbors, but our index is zero based.
                    d = len(upper_neighbors)
                    m_value = (d + 1) / 2

                    for m in [ceil(m_value) - 1, floor(m_value) - 1]:
                        if self.vertex_set[v_k][bk][g][a] == v_k:
                            u_m = upper_neighbors[m]
                            u_m_v_k = u_m + '_' + v_k

                            condition_1 = self.edge_set[u_m_v_k][ct]['type_1']
                            condition_2 = r > self.vertex_set[u_m][pos]

                            if not condition_1 and condition_2:

                                self.vertex_set[u_m][bk][g][a] = v_k
                                self.vertex_set[v_k][bk][g][root] = self.vertex_set[u_m][bk][g][root]
                                self.vertex_set[v_k][bk][g][a] = self.vertex_set[v_k][bk][g][root]
                                r = self.vertex_set[u_m][pos]

    def __vertical_alignment_upper_right__(self, a, bk, ct, l, g, pos, r, root, sf, sk, x, xy):
        for layer_index, layer in enumerate(self.layering):
            r = len(max(self.layering, key=lambda l: len(l)))

            for vertex in reversed(layer):
                v_k = vertex

                # want median of ALL upper neighbors, not just those in the preceding layer
                upper_neighbors = self.__upper_neighbors__(v_k)
                # sort to ensure that they are in order as they appear in the layer
                upper_neighbors = sorted(upper_neighbors, key=lambda pred: self.vertex_set[pred][pos])

                if upper_neighbors:
                    # d is number / index of upper neighbors, but our index is zero based.
                    d = len(upper_neighbors)
                    m_value = (d + 1) / 2

                    for m in [ceil(m_value) - 1, floor(m_value) - 1]:
                        if self.vertex_set[v_k][bk][g][a] == v_k:
                            u_m = upper_neighbors[m]
                            u_m_v_k = u_m + '_' + v_k
                            v_k_u_m = v_k + '_' + u_m

                            condition_1 = False
                            for e in [u_m_v_k, v_k_u_m]:
                                if e in self.edge_set:
                                    condition_1 = self.edge_set[e][ct]['type_1']
                                    if condition_1:
                                        break

                            condition_2 = r > self.vertex_set[u_m][pos]

                            if not condition_1 and condition_2:

                                self.vertex_set[u_m][bk][g][a] = v_k
                                self.vertex_set[v_k][bk][g][root] = self.vertex_set[u_m][bk][g][root]
                                self.vertex_set[v_k][bk][g][a] = self.vertex_set[v_k][bk][g][root]
                                r = self.vertex_set[u_m][pos]

    def __vertical_alignment_lower_left__(self, a, bk, ct, l, g, pos, r, root, sf, sk, x, xy):
        for layer in reversed(self.layering):
            # r is 1 less than the starting index. In Brandes and Koepf they use 1 indexed arrays. Python is 0
            # indexed.
            r = -1

            for vertex_index, vertex in enumerate(layer):
                v_k = vertex

                lower_neighbors = self.vertex_set[v_k]['successors']
                lower_neighbors = sorted(lower_neighbors, key=lambda succ: self.vertex_set[succ][pos])

                if lower_neighbors:
                    # d is number / index of upper neighbors, but our index is zero based.
                    d = len(lower_neighbors)
                    m_value = (d + 1) / 2

                    for m in [floor(m_value) - 1, ceil(m_value) - 1]:

                        if self.vertex_set[v_k][bk][g][a] == v_k:
                            u_m = lower_neighbors[m]
                            v_k_u_m = v_k + '_' + u_m

                            condition_1 = self.edge_set[v_k_u_m][ct]['type_1']
                            condition_2 = r < self.vertex_set[u_m][pos]

                            if not condition_1 and condition_2:
                                self.vertex_set[u_m][bk][g][a] = v_k
                                self.vertex_set[v_k][bk][g][root] = self.vertex_set[u_m][bk][g][root]
                                self.vertex_set[v_k][bk][g][a] = self.vertex_set[v_k][bk][g][root]
                                r = self.vertex_set[u_m][pos]

    def __vertical_alignment_lower_left__(self, a, bk, ct, l, g, pos, r, root, sf, sk, x, xy):
        for layer in reversed(self.layering):
            # r is 1 less than the starting index. In Brandes and Koepf they use 1 indexed arrays. Python is 0
            # indexed.
            r = -1

            for vertex_index, vertex in enumerate(layer):
                v_k = vertex

                lower_neighbors = self.__lower_neighbors__(v_k)
                lower_neighbors = sorted(lower_neighbors, key=lambda succ: self.vertex_set[succ][pos])

                if lower_neighbors:
                    # d is number / index of upper neighbors, but our index is zero based.
                    d = len(lower_neighbors)
                    m_value = (d + 1) / 2

                    for m in [floor(m_value) - 1, ceil(m_value) - 1]:

                        if self.vertex_set[v_k][bk][g][a] == v_k:
                            u_m = lower_neighbors[m]
                            u_m_v_k = u_m + '_' + v_k
                            v_k_u_m = v_k + '_' + u_m

                            condition_1 = False
                            for e in [u_m_v_k, v_k_u_m]:
                                if e in self.edge_set:
                                    condition_1 = self.edge_set[e][ct]['type_1']
                                    if condition_1:
                                        break

                            # condition_1 = self.edge_set[v_k_u_m][ct]['type_1']
                            condition_2 = r < self.vertex_set[u_m][pos]

                            if not condition_1 and condition_2:
                                self.vertex_set[u_m][bk][g][a] = v_k
                                self.vertex_set[v_k][bk][g][root] = self.vertex_set[u_m][bk][g][root]
                                self.vertex_set[v_k][bk][g][a] = self.vertex_set[v_k][bk][g][root]
                                r = self.vertex_set[u_m][pos]

    def __vertical_alignment_lower_right__(self, a, bk, ct, l, g, pos, r, root, sf, sk, x, xy):
        for layer in reversed(self.layering):
            r = len(max(self.layering, key=lambda l: len(l)))

            for vertex in reversed(layer):
                v_k = vertex

                lower_neighbors = self.vertex_set[v_k]['successors']
                lower_neighbors = sorted(lower_neighbors, key=lambda succ: self.vertex_set[succ][pos])

                if lower_neighbors:
                    # d is number / index of upper neighbors, but our index is zero based.
                    d = len(lower_neighbors)
                    m_value = (d + 1) / 2

                    for m in [ceil(m_value) - 1, floor(m_value) - 1]:
                        if self.vertex_set[v_k][bk][g][a] == v_k:
                            u_m = lower_neighbors[m]
                            v_k_u_m = v_k + '_' + u_m

                            condition_1 = self.edge_set[v_k_u_m][ct]['type_1']
                            condition_2 = r > self.vertex_set[u_m][pos]

                            if not condition_1 and condition_2:

                                self.vertex_set[u_m][bk][g][a] = v_k
                                self.vertex_set[v_k][bk][g][root] = self.vertex_set[u_m][bk][g][root]
                                self.vertex_set[v_k][bk][g][a] = self.vertex_set[v_k][bk][g][root]
                                r = self.vertex_set[u_m][pos]

    def __vertical_alignment_lower_right__(self, a, bk, ct, l, g, pos, r, root, sf, sk, x, xy):
        for layer in reversed(self.layering):
            r = len(max(self.layering, key=lambda l: len(l)))

            for vertex in reversed(layer):
                v_k = vertex

                lower_neighbors = self.__lower_neighbors__(v_k)
                lower_neighbors = sorted(lower_neighbors, key=lambda succ: self.vertex_set[succ][pos])

                if lower_neighbors:
                    # d is number / index of upper neighbors, but our index is zero based.
                    d = len(lower_neighbors)
                    m_value = (d + 1) / 2

                    for m in [ceil(m_value) - 1, floor(m_value) - 1]:
                        if self.vertex_set[v_k][bk][g][a] == v_k:
                            u_m = lower_neighbors[m]
                            u_m_v_k = u_m + '_' + v_k
                            v_k_u_m = v_k + '_' + u_m

                            condition_1 = False
                            for e in [u_m_v_k, v_k_u_m]:
                                if e in self.edge_set:
                                    condition_1 = self.edge_set[e][ct]['type_1']
                                    if condition_1:
                                        break

                            # condition_1 = self.edge_set[v_k_u_m][ct]['type_1']
                            condition_2 = r > self.vertex_set[u_m][pos]

                            if not condition_1 and condition_2:

                                self.vertex_set[u_m][bk][g][a] = v_k
                                self.vertex_set[v_k][bk][g][root] = self.vertex_set[u_m][bk][g][root]
                                self.vertex_set[v_k][bk][g][a] = self.vertex_set[v_k][bk][g][root]
                                r = self.vertex_set[u_m][pos]

    def __vertical_alignment__(self, direction="upper_left"):
        # Algorithm 2 from Brandes and Koepf
        # root and align (here called lower-/upper-neighbor) are initalized in vertex_set.__make_alignment_keys__()

        # i = layer_index
        # r = r
        # k = vertex_index
        align = 'align'
        bk = 'brandes_kopf'
        coordinates = 'coordinates'
        crossing_types = 'crossing_types'
        # direction
        layer = 'layer'
        position_in_layer = 'position_in_layer'
        root = 'root'
        shift = 'shift'
        sink = 'sink'

        a = align
        # bk
        # coordinates
        ct = crossing_types
        g = direction # g for greedy_direction
        l = layer
        pos = position_in_layer
        r = root
        sf = shift
        sk = sink
        x = 'x'
        xy = coordinates

        if direction == 'upper_left':
            self.__vertical_alignment_upper_left__(a, bk, ct, l, g, pos, r, root, sf, sk, x, xy)

        if direction == 'upper_right':
            self.__vertical_alignment_upper_right__(a, bk, ct, l, g, pos, r, root, sf, sk, x, xy)

        if direction == 'lower_left':
            self.__vertical_alignment_lower_left__(a, bk, ct, l, g, pos, r, root, sf, sk, x, xy)

        if direction == 'lower_right':
            self.__vertical_alignment_lower_right__(a, bk, ct, l, g, pos, r, root, sf, sk, x, xy)

    def __balance_coordinate_assignments__(self, delta=300, directions=None):
        self.minimum_vertex_separation = delta

        align = 'align'
        brandes_kopf = 'brandes_kopf'
        coordinates = 'coordinates'
        layer = 'layer'
        position_in_layer = 'position_in_layer'
        root = 'root'
        shift = 'shift'
        sink = 'sink'

        a = align
        bk = brandes_kopf
        l = layer
        pos = position_in_layer
        r = root
        sf = shift
        sk = sink
        x = 'x'
        xy = coordinates

        if directions is None:
            directions = ["upper_left", "upper_right", "lower_left", "lower_right"]
            directional_alignment_widths = {"upper_left": 0, "upper_right": 0, "lower_left": 0, "lower_right": 0}

        if directions is not None:
            directional_alignment_widths = {}
            for direction in directions:
                directional_alignment_widths[direction] = 0

        # Find smallest width
        for vertex_key, vertex_values in self.vertex_set.items():
            for direction in directions:
                if vertex_values[bk][direction][xy][x] > directional_alignment_widths[direction]:
                    directional_alignment_widths[direction] = vertex_values[bk][direction][xy][x]

        direction_with_smallest_width = min(directional_alignment_widths, key=directional_alignment_widths.get)
        smallest_direction_width = directional_alignment_widths[direction_with_smallest_width]

        directions_to_realign = [direction for direction in directions if direction != direction_with_smallest_width]

        # Align to smallest width
        for vertex_key, vertex_values in self.vertex_set.items():
            for direction in directions_to_realign:
                this_directions_width = directional_alignment_widths[direction_with_smallest_width]

                try:
                    scaling_factor = smallest_direction_width / this_directions_width
                except ZeroDivisionError:
                    scaling_factor = smallest_direction_width / (this_directions_width + 0.00000000001)
                vertex_values[bk][direction][xy][x] *= scaling_factor

        # Get average median
        average_median_coordinates = []

        for vertex_key, vertex_values in self.vertex_set.items():
            candidate_coordinates = []
            for direction in directions:
                candidate_coordinates.append(vertex_values[bk][direction][xy][x])
            candidate_coordinates.sort()

            k = len(directions)

            lower_median = floor((k + 1) / 2) - 1
            upper_median = ceil((k + 1) / 2) - 1

            average_median = (candidate_coordinates[lower_median] + candidate_coordinates[upper_median]) / 2
            average_median_coordinates.append((vertex_key, average_median))


        self.vertex_set.__make_coordinate_keys__()
        for vertex_key, x_coordinate in average_median_coordinates:
            self.vertex_set[vertex_key]["coordinates"]["x"] = x_coordinate
            self.vertex_set[vertex_key]["coordinates"]["y"] = self.vertex_set[vertex_key][layer] * delta

    def __horizontal_compaction__(self, delta, direction='upper_left'):

            align = 'align'
            brandes_kopf = 'brandes_kopf'
            coordinates = 'coordinates'
            layer = 'layer'
            position_in_layer = 'position_in_layer'
            root = 'root'
            shift = 'shift'
            sink = 'sink'

            a = align
            bk = brandes_kopf
            g = direction  # g for greedy_direction
            l = layer
            pos = position_in_layer
            r = root
            sf = shift
            sk = sink
            x = 'x'
            xy = coordinates

            if direction == 'upper_left':
                for vertex_key, vertex in self.vertex_set.items():
                    if vertex[bk][g][r] == vertex_key:
                        self.vertex_set.__place_block__(vertex_key, delta, direction, self.layering)

                for vertex_key, vertex in self.vertex_set.items():
                    root_of_v = vertex[bk][g][r]

                    vertex[bk][g][xy][x] = self.vertex_set[root_of_v][bk][g][xy][x]

                    sink_root_of_v = self.vertex_set[root_of_v][bk][g][sk]
                    shift_sink_root_of_v = self.vertex_set[sink_root_of_v][bk][g][sf]

                    if vertex_key == root_of_v and not math.isinf(shift_sink_root_of_v):
                        vertex[bk][g][xy][x] += shift_sink_root_of_v

            if direction == 'upper_right':
                for vertex_key, vertex in self.vertex_set.items():
                    if vertex[bk][g][r] == vertex_key:
                        self.vertex_set.__place_block__(vertex_key, delta, direction, self.layering)

                for vertex_key, vertex in self.vertex_set.items():
                    root_of_v = vertex[bk][g][r]

                    vertex[bk][g][xy][x] = self.vertex_set[root_of_v][bk][g][xy][x]

                    sink_root_of_v = self.vertex_set[root_of_v][bk][g][sk]
                    shift_sink_root_of_v = self.vertex_set[sink_root_of_v][bk][g][sf]

                    if vertex_key == root_of_v and not math.isinf(shift_sink_root_of_v):
                        vertex[bk][g][xy][x] += shift_sink_root_of_v


                min_x = min(self.vertex_set.values(), key=lambda v: v[bk][direction][xy][x])[bk][direction][xy][x]

                for vertex_key, vertex in self.vertex_set.items():
                    vertex[bk][g][xy][x] += (min_x * -1)

            if direction == 'lower_left':
                for vertex_key, vertex in self.vertex_set.items():
                    if vertex[bk][g][r] == vertex_key:
                        self.vertex_set.__place_block__(vertex_key, delta, direction, self.layering)

                for vertex_key, vertex in self.vertex_set.items():
                    root_of_v = vertex[bk][g][r]

                    vertex[bk][g][xy][x] = self.vertex_set[root_of_v][bk][g][xy][x]

                    sink_root_of_v = self.vertex_set[root_of_v][bk][g][sk]
                    shift_sink_root_of_v = self.vertex_set[sink_root_of_v][bk][g][sf]

                    if vertex_key == root_of_v and not math.isinf(shift_sink_root_of_v):
                        vertex[bk][g][xy][x] += shift_sink_root_of_v

            if direction == 'lower_right':
                for vertex_key, vertex in self.vertex_set.items():
                    if vertex[bk][g][r] == vertex_key:
                        self.vertex_set.__place_block__(vertex_key, delta, direction, self.layering)

                for vertex_key, vertex in self.vertex_set.items():
                    root_of_v = vertex[bk][g][r]

                    vertex[bk][g][xy][x] = self.vertex_set[root_of_v][bk][g][xy][x]

                    sink_root_of_v = self.vertex_set[root_of_v][bk][g][sk]
                    shift_sink_root_of_v = self.vertex_set[sink_root_of_v][bk][g][sf]

                    if vertex_key == root_of_v and not math.isinf(shift_sink_root_of_v):
                        vertex[bk][g][xy][x] += shift_sink_root_of_v


                min_x = min(self.vertex_set.values(), key=lambda v: v[bk][direction][xy][x])[bk][direction][xy][x]

                for vertex_key, vertex in self.vertex_set.items():
                    vertex[bk][g][xy][x] += (min_x * -1)
