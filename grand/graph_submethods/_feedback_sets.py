from copy import copy, deepcopy
class FeedbackSets:
    # Methods for removing cylces
    def feedback_set(self, implementation='berger_shor_tarjan_modified'):

        #Remove edges between strongly connected components
        if implementation == 'berger_shor_tarjan_modified':
            self.vertex_set.__make_tarjan_keys__()
            vertex_set = deepcopy(self.vertex_set)
            edge_set = deepcopy(self.edge_set)
            graph = self.__class__(vertex_set, edge_set)
            edges_to_reverse = self.berger_shor(graph, implementation)
            del vertex_set
            del edge_set
            del graph

        # Linear Ordering Methods
        if implementation == 'eades_et_al':
            vertex_ordering = self.vertex_set.__eades_et_al__()
            edges_to_reverse = self.edge_set.__feedback_set_from_linear_ordering__(vertex_ordering)

        if implementation == 'magruder_magnitude':
            vertex_ordering = self.vertex_set.__magruder_magnitute__()
            edges_to_reverse = self.edge_set.__feedback_set_from_linear_ordering__(vertex_ordering)

        return edges_to_reverse

    def feedback_arc_set(self, implementation='berger_shor'):
        if implementation == 'berger_shor':
            vertex_set = deepcopy(self.vertex_set)
            edge_set = deepcopy(self.edge_set)
            graph = self.__class__(vertex_set, edge_set)
            edges_to_delete = self.berger_shor(graph)

        return edges_to_delete
