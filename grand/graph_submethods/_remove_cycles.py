class RemoveCycles:
    def remove_cycles(self, method='feedback_set', implementation='berger_shor_tarjan_modified'):
        _methods = ['feedback_set', 'feedback_arc_set', 'linear_ordering']
        _implementations = ['eades_et_al', 'berger_shor', 'berger_shor_tarjan_modified']

        if method == 'feedback_set':
            edges_to_reverse = self.feedback_set(implementation)
            if edges_to_reverse:
                for edge in edges_to_reverse:
                    self.reverse_edge(edge)

        if method == 'feedback_arc_set':
            edges_to_remove = self.feedback_arc_set(implementation)
            if edges_to_remove:
                for edge in edges_to_remove:
                    self.remove_edge(edge)
