from math import inf
from statistics import mean

class AStar:
    def a_star_distance_between(self, a, b):
        return 0

    def a_star_reconstruct_path(self, came_from, current):
        total_path = [current]
        while current in came_from.keys():
            current = came_from[current]
            total_path.append(current)
        total_path.reverse()
        return total_path

    def a_star_heuristic_between(self, a, b):
        heuristic = len(self.vertex_set[a]['successors']) + len(self.vertex_set[b]['successors'])
        return 1 / heuristic

    def a_star_lowest_f_score(self, f_scores, open_set):
        f_min_val = inf
        f_min_key = ''
        for node in open_set:
            val = f_scores[node]
            if val < f_min_val:
                f_min_key = node
                f_min_val = val
        return f_min_key


    def a_star(self, start, stop, dist_func=a_star_distance_between, heur_func=a_star_heuristic_between, direction='successors'):
        if direction not in ['successors', 'predecessors']:
            return []
        closed_set = set()
        open_set = {start}

        came_from = dict()

        v_keys = list(self.vertex_set.keys())

        g_score = {v: inf for v in v_keys}
        g_score[start] = 0

        f_score = {v: inf for v in v_keys}
        f_score[start] = 1

        while open_set:
            current = self.a_star_lowest_f_score(f_score, open_set)
            if current == stop:
                return self.a_star_reconstruct_path(came_from, current)

            closed_set.add(current)
            open_set.remove(current)

            for neighbor in self.vertex_set[current][direction]:
                if neighbor in closed_set:
                    continue
                if neighbor not in open_set:
                    open_set.add(neighbor)

                tentative_g_score = g_score[current] + dist_func(self, current, neighbor)
                if tentative_g_score >= g_score[neighbor]:
                    continue

                came_from[neighbor] = current
                g_score[neighbor] = tentative_g_score
                f_score[neighbor] = tentative_g_score + heur_func(self, neighbor, stop)


    def a_star_depth_lock(self, start, stop, dist_func=a_star_distance_between, heur_func=a_star_heuristic_between, max_depth=3, direction='successors'):
        if direction not in ['successors', 'predecessors']:
            return []

        closed_set = set()
        open_set = {start}

        came_from = dict()

        v_keys = list(self.vertex_set.keys())

        g_score = {v: inf for v in v_keys}
        g_score[start] = 0

        f_score = {v: inf for v in v_keys}
        f_score[start] = 1

        d_score = {v: inf for v in v_keys}
        d_score[start] = 0

        while open_set:
            current = self.a_star_lowest_f_score(f_score, open_set)
            if current == stop:
                return self.a_star_reconstruct_path(came_from, current)



            closed_set.add(current)
            open_set.remove(current)

            if d_score[current] + 1 > max_depth:
                continue

            for neighbor in self.vertex_set[current][direction]:
                if neighbor in closed_set:
                    continue
                if neighbor not in open_set:
                    open_set.add(neighbor)
                    d_score[neighbor] = d_score[current] + 1

                tentative_g_score = g_score[current] + dist_func(self, current, neighbor)
                if tentative_g_score >= g_score[neighbor]:
                    continue

                came_from[neighbor] = current
                g_score[neighbor] = tentative_g_score
                f_score[neighbor] = tentative_g_score + heur_func(self, neighbor, stop)



    def a_star_multi_target_depth_lock(self, start, stops, dist_func=a_star_distance_between, heur_func=a_star_heuristic_between, max_depth=3, direction='successors'):
        stops_paths = {stop: None for stop in stops}

        if direction not in ['successors', 'predecessors']:
            return stops_paths

        closed_set = set()
        open_set = {start}

        came_from = dict()

        v_keys = list(self.vertex_set.keys())

        g_score = {v: inf for v in v_keys}
        g_score[start] = 0

        f_score = {v: inf for v in v_keys}
        f_score[start] = 1

        d_score = {v: inf for v in v_keys}
        d_score[start] = 0

        while open_set:
            current = self.a_star_lowest_f_score(f_score, open_set)
            if current in stops:
                stops_paths[current] = self.a_star_reconstruct_path(came_from, current)
                if all([v != None for k, v in stops_paths.items()]):
                    return stops_paths


            closed_set.add(current)
            open_set.remove(current)

            if d_score[current] + 1 > max_depth:
                continue

            for neighbor in self.vertex_set[current][direction]:
                if neighbor in closed_set:
                    continue
                if neighbor not in open_set:
                    open_set.add(neighbor)
                    d_score[neighbor] = d_score[current] + 1

                tentative_g_score = g_score[current] + dist_func(self, current, neighbor)
                if tentative_g_score >= g_score[neighbor]:
                    continue

                came_from[neighbor] = current
                g_score[neighbor] = tentative_g_score
                f_score[neighbor] = tentative_g_score + mean([heur_func(self, neighbor, stop) for stop in stops if stops_paths[stop] is None]+[0 for stop in stops if stops_paths[stop] is not None])
