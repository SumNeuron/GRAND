class Layering:
    # Methods for making a layering
    def make_layering(self, method='healy_nikolov', implementation=''):
        layering = self.vertex_set.__make_layering__(method=method, implementation=implementation)
        self.layering = layering
        self.number_of_layers = len(layering)

        self.set_spans()
