import math
from copy import copy, deepcopy
from itertools import chain, groupby
from math import floor, ceil
from operator import itemgetter
from random import shuffle
from statistics import median_high, median_low

# self defined functions
from .helpers.tally import tally

# import dependencies
from .edge import Edge
from .edgeset import EdgeSet
from .svg import SVG
from .vertex import Vertex
from .vertexset import VertexSet


# import large methods of Graph class

# horziontal coordinate assignment for heirarchical layered graphs
from .graph_submethods._brandes_kopf_2002 import BrandesKopf2002
# determine cyclic edges - required to be removed for most algorithms
from .graph_submethods._berger_shor_1990 import BergerShor1990
# sugiyama method of making properly layered graph
from .graph_submethods._sugiyama_tagawa_toda_1981 import SugiyamaTagawaToda1981
# eades wormald median edge crossing reduction
from .graph_submethods._eades_wormald_1994 import EadesWormald1994
# snake method for crossing minimization
from .graph_submethods._magruder_2017 import Snake
# depth first search
from .graph_submethods._depth_first_search import DepthFirstSearch
# a*
from .graph_submethods._a_star import AStar
# Dijkstra
from .graph_submethods._dijkstra import Dijkstra
# calulcate the number of edge crossing
from .graph_submethods._calculate_edge_crossings import CalculateEdgeCrossings

# remove cycles
from .graph_submethods._remove_cycles import RemoveCycles
# get edge keys from removed edges and all dummy edges for those edges
from .graph_submethods._recover_removed_edges import RecoverRemovedEdges
# get feedback and feedback arc sets
from .graph_submethods._feedback_sets import FeedbackSets
# call corresponding layeing function
from .graph_submethods._layering import Layering

# Miscellaneous
from .graph_submethods._post_drawing import PostDrawing

# calculate y position and such
from .graph_submethods._drawing_aids import DrawingAids
from .graph_submethods._helpers import Helpers
from .graph_submethods._reconstruct_from_paths import ReconFromPaths

class Graph(
        BrandesKopf2002, BergerShor1990, SugiyamaTagawaToda1981,
        EadesWormald1994, DepthFirstSearch, RecoverRemovedEdges,
        DrawingAids, RemoveCycles, FeedbackSets, Helpers,
        CalculateEdgeCrossings, Layering, AStar, Dijkstra, PostDrawing,
        ReconFromPaths
    ):
    def __init__(self, vertices = [], edges = []):
        """
        Initialize a graph object. The easiest** way to make a graph is to give it a list of vertices (just IDs) and
        a list of lists representing edges. If the edges are meant to be ordered, then they should be input as such
        e.g. an edge[i,j] represents (i o---> j).

        ** this holds if you are transferring data / output from another program or function. Naturally, one can
        also give the graph the VertexSet and EdgeSet objects. Alternatively a list of Vertex / Edge objects will
        suffice.
        """
        # Logic Flow:
        # Test if the arguments vertices and edges are lists.
        # ---- If: the first element in each list are Vertex or Edge object, pass them to VertexSet and EdgeSet
        #      constructors.
        # ---- Else: convert list of IDs into Vertex and Edge Objects
        # Else: arguments are VertexSet and EdgeSet objects.

        # Default arguments which are empty.
        if not vertices and not edges:
            self.vertex_set,  self.edge_set = VertexSet(), EdgeSet()
            self.layering, self.removed_edges = [], EdgeSet()
            self.removed_vertices = VertexSet()

        # Passing VertexSet and EdgeSet
        elif type(vertices) == VertexSet and type(edges) == EdgeSet:
            self.vertex_set, self.edge_set = vertices, edges
            self.layering, self.removed_edges = [], EdgeSet()
            self.removed_vertices = VertexSet()


        # Passed named tuples
        elif (type(vertices[0]) == tuple and type(edges[0]) == tuple) and \
             (type(vertices[0][1]) == Vertex and type(edges[0][1]) == Edge):

            to_add = {}
            for vertex_tuple in vertices:
                to_add[vertex_tuple[0]] = vertex_tuple[1]
            self.vertex_set = VertexSet(**to_add)

            to_add = {}
            for edge_tuple in edges:
                to_add[edge_tuple[0]] = edge_tuple[1]
            self.edge_set = EdgeSet(**to_add)

        # Passed arguments are not empty, not a VertexSet or EdgeSet nor named tuples
        elif type(vertices[0]) == Vertex and type(edges[0]) == Edge:
            try:
                to_add = {}
                for vertex in vertices:
                    to_add[vertex['name']] = vertex
                self.vertex_set = VertexSet(**to_add)
            except KeyError:
                print("Vertices did not contain a 'name' key. Vertices initialized with"
                      " index based 'names'.")
                to_add = {}
                for index, vertex in enumerate(vertices):
                    to_add[index] = vertex
                self.vertex_set = VertexSet(**to_add)

            try:
                to_add = {}
                for edge in edges:
                    edge_key = edge['source'] + "_" + edge['target']
                    i = 1
                    while edge_key in to_add.keys():
                        i += 1
                        edge_key = edge['source'] + i * '_' + edge['target']
                    to_add[edge_key] = edge
                self.edge_set = EdgeSet(**to_add)
            except KeyError:
                print("Edges do not have complete information. Initializing an empty edge set instead.")
                self.edge_set = EdgeSet()

            self.layering = []
            self.removed_edges = EdgeSet()
            self.removed_vertices = VertexSet()


        # Assume then it is just lists of strings and such
        else:
            self.vertex_set = VertexSet()
            self.edge_set = EdgeSet()
            for vertex in vertices:
                self.vertex_set[str(vertex)] = Vertex()

            edges = self.preprocess_input(vertices, edges)
            for edge in edges:
                v, w = edge[0], edge[1]
                edge_key = str(v) + "_" + str(w)
                i = 1
                while edge_key in self.edge_set:
                    i += 1
                    edge_key = str(v) + i * '_' + str(w)
                self.edge_set[edge_key] = Edge(**{'source':str(v), 'target':str(w)})

                self.vertex_set[str(v)]['successors'].append(str(w))
                self.vertex_set[str(w)]['predecessors'].append(str(v))

            self.layering = []
            self.removed_edges = EdgeSet()
            self.removed_vertices = VertexSet()


        self.number_of_crossings = None
        self.number_of_layers = 0



    def preprocess_input(self, vertices, edges):
        # removes self loops

        buckles = [[u, w] for (u, w) in edges if u == w]
        edges = [edge for edge in edges if edge not in buckles]
        excluded_edges = []


        tallied_edges = tally(edges)
        for edge_key, number_of in tallied_edges:
            if number_of > 1:
                [excluded_edges.append(edge_key) for i in range(number_of - 1)]

        edges = [key for key, iterator_thing in groupby(edges)]

        self.buckles = buckles
        self.excluded_edges = excluded_edges
        # print('buckles:', buckles)
        # print('excluded edges:', excluded_edges)
        return edges



    def __swap__(self, vertex_1, vertex_2):
        position_1 = self.vertex_set[vertex_1]['position_in_layer']
        position_2 = self.vertex_set[vertex_2]['position_in_layer']

        self.vertex_set[vertex_1]['position_in_layer'] = position_2
        self.vertex_set[vertex_2]['position_in_layer'] = position_1

        layer_index = self.vertex_set[vertex_1]['layer']
        layer = self.layering[layer_index]

        layer[position_1] = vertex_2
        layer[position_2] = vertex_1

        self.layering[layer_index] = layer



    def parse_paths(self, list_lists_of_paths):
        vertices = []
        edges = []
        for lists_of_paths in list_lists_of_paths:
            if lists_of_paths is []:
                continue
            for path in lists_of_paths:
                old_vertex = None
                for vertex in path:
                    if vertex not in vertices:
                        vertices.append(vertex)
                    if old_vertex is not None:
                        edges.append([old_vertex, vertex])
                    old_vertex = vertex
        return vertices, edges



    def extended_neighborhood(self, of, scope=1):
        neighborhood = [str(of)]
        used = []
        it = 0

        scopes = [[] for i in range(scope + 1)]
        scopes[0].append(str(of))
        for (i, neighborhood_scope) in enumerate(scopes):
            if i == len(scopes) - 1:
                break

            for neighbor in neighborhood_scope:
                successors = self.vertex_set[neighbor]['successors']
                predecessors = self.vertex_set[neighbor]['predecessors']
                new_neighbors = successors + predecessors
                for new_neighbor in new_neighbors:
                    if new_neighbor not in scopes[i+1]:
                        scopes[i + 1].append(new_neighbor)
        flattened_scopes = [item for local_scope in scopes for item in local_scope]
        return flattened_scopes
